package com.sparrow.dex.tools;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.audio.Music;
import com.badlogic.gdx.audio.Sound;

/**
 * Created by sparrow on 01.02.17.
 */

public class Singleton {
    private static Singleton singleton;
    private Sound fallSound;
    private Sound jumpSound;
    private Sound scoreSound;
    private Music themeMusic;
    private Music menuMusic;

    private Singleton(){
        fallSound = Gdx.audio.newSound(Gdx.files.internal("music/fall.mp3"));
        jumpSound = Gdx.audio.newSound(Gdx.files.internal("music/jump.mp3"));
        scoreSound = Gdx.audio.newSound(Gdx.files.internal("music/score.wav"));
        themeMusic = Gdx.audio.newMusic(Gdx.files.internal("music/theme.mp3"));
        themeMusic.setLooping(true);
        themeMusic.setVolume(0.3f);
        menuMusic = Gdx.audio.newMusic(Gdx.files.internal("music/menuMusic.mp3"));
        menuMusic.setLooping(true);
        menuMusic.setVolume(0.3f);
    }

    public static Singleton getInstance(){
        if(singleton == null){
            singleton = new Singleton();
        }
        return singleton;
    }

    public void playFallSound() {
        jumpSound.stop();
        themeMusic.stop();
        fallSound.play(0.4f);
    }

    public void playJumpSound() {
        jumpSound.play(0.4f);
    }

    public void playScoreSound() {
        scoreSound.play(0.6f);
    }

    public void playThemeMusic() {
        menuMusic.pause();
        themeMusic.play();
    }

    public void playMenuMusic() {
        menuMusic.play();
    }

    public void dispose(){
        fallSound.dispose();
        jumpSound.dispose();
        scoreSound.dispose();
        themeMusic.dispose();
        menuMusic.dispose();
    }
}
