package com.sparrow.dex.sprites;

import com.badlogic.gdx.graphics.g2d.TextureAtlas;
import com.badlogic.gdx.graphics.g2d.TextureRegion;
import com.sun.prism.Texture;

import java.util.Iterator;

/**
 * Created by sparrow on 31.01.17.
 */

public class FrameIterator implements Iterator {
    private TextureAtlas atlas;
    private String regionName;
    private int i;

    public FrameIterator(TextureAtlas atlas, String regionName){
        this.atlas = atlas;
        this.regionName = regionName;
        i=1;
    }

    @Override
    public boolean hasNext() {
        return atlas.findRegion(regionName, i) == null;
    }

    @Override
    public TextureRegion next() {
        return new TextureRegion(atlas.findRegion(regionName, i++));
    }

    @Override
    public void remove() {}

    public void goToFirstFrame(){
        i = 1;
    }
}
