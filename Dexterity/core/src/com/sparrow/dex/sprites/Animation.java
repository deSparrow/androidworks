package com.sparrow.dex.sprites;

import com.badlogic.gdx.graphics.g2d.TextureAtlas;
import com.badlogic.gdx.graphics.g2d.TextureRegion;

/**
 * Created by sparrow on 31.01.17.
 */

public class Animation {
    private FrameIterator frameIterator;
    private TextureRegion currentFrame;
    private static final float maxFrameTime = 0.06f;
    private float currentFrameTime;

    public Animation(TextureAtlas atlas, String regionName){
        frameIterator = new FrameIterator(atlas, regionName);
        currentFrame = atlas.findRegion("red_bird", 1);
    }

    public void update(float deltaTime){
        currentFrameTime += deltaTime;
        if(currentFrameTime > maxFrameTime){
            if(!frameIterator.hasNext()){
                currentFrame = frameIterator.next();
            } else{
                frameIterator.goToFirstFrame();
                currentFrame = frameIterator.next();
            }
            currentFrameTime = 0;
        }
    }

    public TextureRegion getCurrentFrame(){
        return currentFrame;
    }
}
