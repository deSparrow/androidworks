package com.sparrow.dex.sprites;

import com.badlogic.gdx.graphics.Color;
import com.badlogic.gdx.graphics.g2d.freetype.FreeTypeFontGenerator;

/**
 * Created by sparrow on 15.02.17.
 */

public class PlainLabel extends Label {
    public PlainLabel(int size, Color color, Color borderColor, int borderWidth, Counter.FONT_TYPE font_type){
        FreeTypeFontGenerator.FreeTypeFontParameter parameter = new FreeTypeFontGenerator.FreeTypeFontParameter();
        parameter.size = size;
        parameter.color = color;
        parameter.borderColor = borderColor;
        parameter.borderWidth = borderWidth;
        makeFont(parameter, font_type);
    }

    @Override
    public String getText() {
        return "Tap screen\nto move\nthe bird!";
    }
}
