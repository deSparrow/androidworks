package com.sparrow.dex.sprites;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.Preferences;
import com.badlogic.gdx.graphics.Color;
import com.badlogic.gdx.graphics.g2d.BitmapFont;
import com.badlogic.gdx.graphics.g2d.freetype.FreeTypeFontGenerator;
import com.sparrow.dex.Game;

/**
 * Created by sparrow on 12.02.17.
 */

public class HiScoreLabel extends Label {
    public static final String KEY_HISCORE = "com.sparrow.dex.preferences.hiscore";
    private int hiScore;
    private Preferences preferences;

    public HiScoreLabel(int size, Color color, Color borderColor, int borderWidth, Counter.FONT_TYPE font_type){
        FreeTypeFontGenerator.FreeTypeFontParameter parameter = new FreeTypeFontGenerator.FreeTypeFontParameter();
        parameter.size = size;
        parameter.color = color;
        parameter.borderColor = borderColor;
        parameter.borderWidth = borderWidth;
        makeFont(parameter, font_type);

        preferences = Gdx.app.getPreferences(Game.PREFERENCES_KEY);
        if(preferences.contains(KEY_HISCORE)){
            hiScore = preferences.getInteger(KEY_HISCORE);
        }else {
            hiScore = 0;
            preferences.putInteger(KEY_HISCORE, hiScore);
            preferences.flush();
        }
    }

    @Override
    public String getText() {
        return "High Score: "+hiScore;
    }
}
