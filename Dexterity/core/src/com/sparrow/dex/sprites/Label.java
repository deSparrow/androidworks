package com.sparrow.dex.sprites;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.graphics.g2d.BitmapFont;
import com.badlogic.gdx.graphics.g2d.freetype.FreeTypeFontGenerator;
import com.badlogic.gdx.utils.Disposable;

/**
 * Created by sparrow on 12.02.17.
 */

public abstract class Label implements Disposable{
    public static final int FONT_SIZE = 24;
    public static final int FONT_SIZE_SMALL = 20;
    protected BitmapFont font;

    public enum FONT_TYPE {
        ROBOTO_LIGHT,
        AMIGA4EVER
    }

    public abstract String getText();

    public BitmapFont getFont(){
        return font;
    }

    protected BitmapFont generateFont(FreeTypeFontGenerator.FreeTypeFontParameter parameter, String fontReference){
        FreeTypeFontGenerator generator = new FreeTypeFontGenerator(Gdx.files.internal(fontReference));
        BitmapFont font = generator.generateFont(parameter);
        generator.dispose();
        return font;
    }

    protected void makeFont(FreeTypeFontGenerator.FreeTypeFontParameter parameter, Counter.FONT_TYPE font_type){
        String path;
        switch (font_type){
            case ROBOTO_LIGHT:
                path = "fonts/roboto_light.ttf";
                break;

            default:
                path = "fonts/amiga4ever pro2.ttf";
        }
        font = generateFont(parameter, path);
    }

    @Override
    public void dispose() {
        font.dispose();
    }
}
