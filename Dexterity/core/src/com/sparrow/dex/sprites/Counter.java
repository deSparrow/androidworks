package com.sparrow.dex.sprites;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.Preferences;
import com.badlogic.gdx.graphics.Color;
import com.badlogic.gdx.graphics.g2d.freetype.FreeTypeFontGenerator;
import com.sparrow.dex.Game;

/**
 * Created by sparrow on 06.02.17.
 */

public class Counter extends Label {
    private int score;
    private int hiScore;
    private Preferences preferences;

    public Counter(int size, Color color, Color borderColor, int borderWidth, FONT_TYPE font_type){
        FreeTypeFontGenerator.FreeTypeFontParameter parameter = new FreeTypeFontGenerator.FreeTypeFontParameter();
        parameter.size = size;
        parameter.color = color;
        parameter.borderColor = borderColor;
        parameter.borderWidth = borderWidth;
        makeFont(parameter, font_type);

        preferences = Gdx.app.getPreferences(Game.PREFERENCES_KEY);
        hiScore = preferences.getInteger(HiScoreLabel.KEY_HISCORE);
    }

    @Override
    public String getText() {
        return "High Score: "+hiScore+"\n\nScore: "+score;
    }

    public void updateScore(){
        ++score;
    }

    public void updateHiScore() {
        if(hiScore < score){
            hiScore = score;
            preferences.putInteger(HiScoreLabel.KEY_HISCORE, hiScore);
            preferences.flush();
        }
    }
}
