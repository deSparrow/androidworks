package com.sparrow.dex.sprites;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.audio.Sound;
import com.badlogic.gdx.graphics.g2d.TextureAtlas;
import com.badlogic.gdx.graphics.g2d.TextureRegion;
import com.badlogic.gdx.math.Circle;
import com.badlogic.gdx.math.Rectangle;
import com.badlogic.gdx.math.Vector3;
import com.badlogic.gdx.utils.Array;
import com.badlogic.gdx.graphics.Texture;
import com.sparrow.dex.tools.Singleton;

/**
 * Created by sparrow on 27.01.17.
 */

public class Bird {
    public static final int GRAVITY_MEDIUM = -40;
    public static int GRAVITY;
    private static final int MOVEMENT = 120;
    private Vector3 position;
    private Vector3 velocity;
    private Rectangle rectangle;

    private TextureAtlas textureAtlas;

    private Animation animation;

    public Bird(int x, int y){
        position = new Vector3(x, y, 0);
        velocity = new Vector3(0, 0, 0);

        textureAtlas = new TextureAtlas("red_bird.pack");
        rectangle = new Rectangle(x+10, y+25, textureAtlas.findRegion("red_bird", 1).getRegionWidth()-5, textureAtlas.findRegion("red_bird", 1).getRegionHeight()-10);

        animation = new Animation(textureAtlas, "red_bird");
    }

    public void update(float deltaTime){
        animation.update(deltaTime);
        if(position.y > 0) {
            velocity.add(0, GRAVITY, 0);
        }
        velocity.scl(deltaTime);
        position.add(MOVEMENT*deltaTime, velocity.y, 0);

        velocity.scl(1/deltaTime);
        if(position.y>745){
            position.y = 745;
            velocity.y = 0;
        }
        rectangle.setPosition(position.x, position.y);
    }

    public Vector3 getPosition() {
        return position;
    }

    public TextureRegion getBirdTexture() {
        return animation.getCurrentFrame();
    }

    public void jump(){
        Singleton.getInstance().playJumpSound();
        velocity.y+=3000;
        if(velocity.y > 600){
            velocity.y = 600;
        }
    }

    public Rectangle getCircleBody(){
        return rectangle;
    }

    public void dispose(){
        textureAtlas.dispose();
    }
}
