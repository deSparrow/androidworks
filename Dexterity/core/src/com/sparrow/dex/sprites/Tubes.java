package com.sparrow.dex.sprites;

import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.math.RandomXS128;
import com.badlogic.gdx.math.Rectangle;
import com.badlogic.gdx.math.Vector2;

/**
 * Created by sparrow on 27.01.17.
 */

public class Tubes {
    public static final int TUBE_WIDTH = 32;
    private static int RANGE_OF_TUBES = 256;
    private static int WIDTH_OF_GAP = 128;
    private static int LOWEST_OPENING = 180;
    private Texture topTube;
    private Texture downTube;
    private Vector2 topTubePosition;
    private Vector2 downTubePosition;
    private RandomXS128 random;
    private Rectangle topRect, downRect;
    private int gapIntergralD;
    private boolean passed;

    public Tubes(float x, float birdPositionx){
        topTube = new Texture("pipes/lightsaber_red_up.png");
        downTube = new Texture("pipes/lightsaber_red_down.png");
        random = new RandomXS128();
        gapIntergralD = random.nextInt(80) + WIDTH_OF_GAP;

        topTubePosition = new Vector2(birdPositionx+WIDTH_OF_GAP+x, random.nextInt(RANGE_OF_TUBES)+gapIntergralD+LOWEST_OPENING);
        downTubePosition = new Vector2(birdPositionx+WIDTH_OF_GAP+x, topTubePosition.y-gapIntergralD-downTube.getHeight());

        topRect = new Rectangle(topTubePosition.x, topTubePosition.y, topTube.getWidth(), topTube.getHeight());
        downRect = new Rectangle(downTubePosition.x, downTubePosition.y, downTube.getWidth(), downTube.getHeight()-20);

    }

    public Texture getTopTube() {
        return topTube;
    }

    public Texture getDownTube() {
        return downTube;
    }

    public Vector2 getTopTubePosition() {
        return topTubePosition;
    }

    public Vector2 getDownTubePosition() {
        return downTubePosition;
    }

    public void reposition(float x){
        gapIntergralD = random.nextInt(50) + WIDTH_OF_GAP;
        topTubePosition.set(x, random.nextInt(RANGE_OF_TUBES)+gapIntergralD+LOWEST_OPENING);
        downTubePosition.set(x, topTubePosition.y-gapIntergralD-downTube.getHeight());
        topRect = new Rectangle(topTubePosition.x, topTubePosition.y, topTube.getWidth(), topTube.getHeight());
        downRect = new Rectangle(downTubePosition.x, downTubePosition.y, downTube.getWidth(), downTube.getHeight()-20);
        passed = false;
    }

    public boolean collides(Rectangle flyingObject){
        return flyingObject.overlaps(topRect) || flyingObject.overlaps(downRect);
    }

    public boolean isPassed() {
        return passed;
    }

    public void setPassed(boolean passed) {
        this.passed = passed;
    }

    public void dispose(){
        topTube.dispose();
        downTube.dispose();
    }
}
