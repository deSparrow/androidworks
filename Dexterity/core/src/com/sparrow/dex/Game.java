package com.sparrow.dex;

import com.badlogic.gdx.ApplicationAdapter;
import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.audio.Music;
import com.badlogic.gdx.audio.Sound;
import com.badlogic.gdx.graphics.GL20;
import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.sparrow.dex.states.GameStateManager;
import com.sparrow.dex.states.MenuState;
import com.sparrow.dex.tools.Singleton;

public class Game extends ApplicationAdapter {
	public static final int WIDTH = 480;
	public static final int HEIGHT = 800;
	public static final String GAME_TITLE = "Flappy birds";
	public static final String PREFERENCES_KEY = "com.sparrow.dex.preferences";

    private GameStateManager stateManager;
    private SpriteBatch batch;

	@Override
	public void create () {
		batch = new SpriteBatch();
        stateManager = new GameStateManager();
        stateManager.push(new MenuState(stateManager));
        Gdx.gl.glClearColor(1, 0, 0, 1);
	}

	@Override
	public void render () {
		Gdx.gl.glClear(GL20.GL_COLOR_BUFFER_BIT);
		stateManager.update(Gdx.graphics.getDeltaTime());
        stateManager.render(batch);
	}
	
	@Override
	public void dispose () {
		batch.dispose();
		Singleton.getInstance().dispose();
	}
}
