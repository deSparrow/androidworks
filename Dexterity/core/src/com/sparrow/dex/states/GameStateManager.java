package com.sparrow.dex.states;

import com.badlogic.gdx.graphics.g2d.SpriteBatch;

import java.util.Stack;

/**
 * Created by sparrow on 27.01.17.
 */
public class GameStateManager {
    private Stack<State> stateStack;

    public GameStateManager(){
        stateStack = new Stack<State>();
    }

    public void push(State state){
        stateStack.push(state);
    }

    public void pop(){
        stateStack.pop().dispose();
    }

    public void set(State state){
        stateStack.pop().dispose();
        stateStack.push(state);
    }

    public void update(float deltaTime){
        stateStack.peek().update(deltaTime);
    }

    public void render(SpriteBatch batch){
        stateStack.peek().render(batch);

    }
}
