package com.sparrow.dex.states;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.graphics.Color;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.badlogic.gdx.graphics.Texture;
import com.sparrow.dex.Game;
import com.sparrow.dex.sprites.Counter;
import com.sparrow.dex.sprites.HiScoreLabel;
import com.sparrow.dex.sprites.Label;
import com.sparrow.dex.tools.Singleton;

/**
 * Created by sparrow on 27.01.17.
 */

public class MenuState extends State {
    private Texture background;
    private Texture playbutton;
    private HiScoreLabel hiScoreLabel;

    public MenuState(GameStateManager stateManager) {
        super(stateManager);
        Singleton.getInstance().playMenuMusic();
        background = new Texture("background_dark_london.jpg");
        playbutton = new Texture("PlayButton.png");
        hiScoreLabel = new HiScoreLabel(Label.FONT_SIZE, Color.valueOf("DDEEF6"), Color.valueOf("3D7C99"), Label.FONT_SIZE/10, Label.FONT_TYPE.AMIGA4EVER);
        camera.setToOrtho(false, Game.WIDTH, Game.HEIGHT);
    }

    @Override
    public void update(float deltaTime) {
        handleInput();
    }

    @Override
    public void render(SpriteBatch batch) {
        batch.begin();
        batch.setProjectionMatrix(camera.combined);
        batch.draw(background, 0, 0);
        batch.draw(playbutton, camera.position.x-playbutton.getWidth()/2, camera.position.y);
        hiScoreLabel.getFont().draw(batch, hiScoreLabel.getText(), camera.position.x - camera.viewportWidth/2 + 8, Game.HEIGHT - Label.FONT_SIZE/2);
        batch.end();

    }

    @Override
    protected void handleInput() {
        if(Gdx.input.justTouched()){
            stateManager.set(new PlayState(stateManager));
        }
    }

    @Override
    public void dispose() {
        background.dispose();
        playbutton.dispose();
        hiScoreLabel.dispose();
    }
}
