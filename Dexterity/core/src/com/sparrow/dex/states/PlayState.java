package com.sparrow.dex.states;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.graphics.Color;
import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.badlogic.gdx.math.Vector2;
import com.badlogic.gdx.utils.Array;
import com.sparrow.dex.Game;
import com.sparrow.dex.sprites.Bird;
import com.sparrow.dex.sprites.Counter;
import com.sparrow.dex.sprites.Label;
import com.sparrow.dex.sprites.PlainLabel;
import com.sparrow.dex.sprites.Tubes;
import com.sparrow.dex.tools.Singleton;

/**
 * Created by sparrow on 27.01.17.
 */

public class PlayState extends State {
    private static final int GROUND_OFFSET = -30;
    private static final int TUBE_SPACING = 256;
    private static final int TUBE_COUNT = 4;
    private Bird bird;
    private Texture background;
    private Texture ground;
    private Vector2 groundP1, groundP2;
    private Counter scoreCounter;
    private Array<Tubes> tubesArray;
    private boolean isReleased;
    private PlainLabel infoLabel;

    public PlayState(GameStateManager stateManager) {
        super(stateManager);
        scoreCounter = new Counter(Label.FONT_SIZE, Color.valueOf("DDEEF6"), Color.valueOf("3D7C99"), Label.FONT_SIZE/10, Label.FONT_TYPE.AMIGA4EVER);
        infoLabel = new PlainLabel(Label.FONT_SIZE_SMALL, Color.valueOf("DDEEF6"), Color.valueOf("5C5876"), Label.FONT_SIZE_SMALL/10, Label.FONT_TYPE.AMIGA4EVER);
        background = new Texture("background_dark_london.jpg");
        bird = new Bird(30, 400);
        Bird.GRAVITY = 0;
        camera.setToOrtho(false, Game.WIDTH, Game.HEIGHT);
        tubesArray = new Array<Tubes>();
        ground = new Texture("ground.png");
        groundP1 = new Vector2(camera.position.x - camera.viewportWidth, GROUND_OFFSET);
        groundP2 = new Vector2(camera.position.x - camera.viewportWidth + ground.getWidth(), GROUND_OFFSET);
        camera.position.x = bird.getPosition().x + 110;
        camera.update();
        Singleton.getInstance().playThemeMusic();
    }

    @Override
    public void update(float deltaTime) {
        handleInput();
        updateGround();
        bird.update(deltaTime);
        camera.position.x = bird.getPosition().x + 110;

        for (int i=0; i<tubesArray.size; i++){
            Tubes t = tubesArray.get(i);
            if(camera.position.x - (camera.viewportWidth)/2 > t.getTopTubePosition().x + t.getTopTube().getWidth()){
                t.reposition(t.getTopTubePosition().x + ((Tubes.TUBE_WIDTH + TUBE_SPACING)*TUBE_COUNT));
            }
            if(t.collides(bird.getCircleBody())){
                gameOver();
                return;
            } else if(t.getTopTubePosition().x < bird.getPosition().x){
                if(!t.isPassed()){
                    Singleton.getInstance().playScoreSound();
                    scoreCounter.updateScore();
                    t.setPassed(true);
                }
            }
        }

        if(bird.getPosition().y <= ground.getHeight() + GROUND_OFFSET){
            gameOver();
            return;
        }
        camera.update();
    }

    private void gameOver() {
        Singleton.getInstance().playFallSound();
        scoreCounter.updateHiScore();
        stateManager.set(new MenuState(stateManager));
    }

    @Override
    public void render(SpriteBatch batch) {
        batch.setProjectionMatrix(camera.combined);
        batch.begin();
        batch.draw(background, camera.position.x - camera.viewportWidth/2, 0, Game.WIDTH, Game.HEIGHT);
        batch.draw(bird.getBirdTexture(), bird.getPosition().x, bird.getPosition().y);

        for (Tubes tube : tubesArray) {
            batch.draw(tube.getTopTube(), tube.getTopTubePosition().x, tube.getTopTubePosition().y);
            batch.draw(tube.getDownTube(), tube.getDownTubePosition().x, tube.getDownTubePosition().y);
        }

        batch.draw(ground, groundP1.x, groundP1.y);
        batch.draw(ground, groundP2.x, groundP2.y);
        scoreCounter.getFont().draw(batch, scoreCounter.getText(), camera.position.x - camera.viewportWidth/2 + 8, Game.HEIGHT - Label.FONT_SIZE/2);
        if(!isReleased){
            infoLabel.getFont().draw(batch, infoLabel.getText(), camera.position.x - camera.viewportWidth/8 + 8, Game.HEIGHT/2 - 8);
        }
        batch.end();
    }

    private void updateGround(){
        if(camera.position.x - camera.viewportWidth/2 > groundP1.x + ground.getWidth()){
            groundP1.add(ground.getWidth()*2, 0);
        }
        if(camera.position.x - camera.viewportWidth/2 > groundP2.x + ground.getWidth()){
            groundP2.add(ground.getWidth()*2, 0);
        }
    }

    @Override
    protected void handleInput() {
        if(Gdx.input.justTouched()){
            if(!isReleased){
                Bird.GRAVITY = Bird.GRAVITY_MEDIUM;
                isReleased = true;
                infoLabel.dispose();
                for(int i = 1; i<=TUBE_COUNT; i++){
                    tubesArray.add(new Tubes(i*(TUBE_SPACING+Tubes.TUBE_WIDTH), bird.getPosition().x));
                }
            }
            bird.jump();
        }
    }

    @Override
    public void dispose() {
        background.dispose();
        ground.dispose();
        bird.dispose();
        scoreCounter.dispose();
        for(Tubes t:tubesArray){
            t.dispose();
        }
    }
}
