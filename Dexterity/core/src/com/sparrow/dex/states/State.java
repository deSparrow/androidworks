package com.sparrow.dex.states;

import com.badlogic.gdx.graphics.OrthographicCamera;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.badlogic.gdx.math.Vector3;

/**
 * Created by sparrow on 27.01.17.
 */

public abstract class State {
    protected OrthographicCamera camera;
    protected Vector3 mouse;
    protected GameStateManager stateManager;

    public State(GameStateManager stateManager) {
        camera = new OrthographicCamera();
        mouse = new Vector3();
        this.stateManager = stateManager;
    }

    public abstract void update(float deltaTime);

    public abstract void render(SpriteBatch batch);

    protected abstract void handleInput();

    public abstract void dispose();

}
