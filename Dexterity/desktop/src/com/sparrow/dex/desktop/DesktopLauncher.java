package com.sparrow.dex.desktop;

import com.badlogic.gdx.Files;
import com.badlogic.gdx.backends.lwjgl.LwjglApplication;
import com.badlogic.gdx.backends.lwjgl.LwjglApplicationConfiguration;
import com.sparrow.dex.Game;

public class DesktopLauncher {
	public static void main (String[] arg) {
		LwjglApplicationConfiguration config = new LwjglApplicationConfiguration();
		config.width = Game.WIDTH;
		config.height = Game.HEIGHT;
		config.title = Game.GAME_TITLE;
		config.resizable = false;
		config.addIcon("../res/drawable-xhdpi/ic_launcher.png", Files.FileType.Internal);
		new LwjglApplication(new Game(), config);
	}
}
