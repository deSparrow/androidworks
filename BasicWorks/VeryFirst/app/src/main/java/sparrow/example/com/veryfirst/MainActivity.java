package sparrow.example.com.veryfirst;

import android.content.Context;
import android.content.SharedPreferences;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import java.util.Random;

public class MainActivity extends AppCompatActivity {
    private int correctNumber;
    private int counter;
    SharedPreferences sharedPreferences;
    SharedPreferences.Editor editor;

    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        sharedPreferences = getSharedPreferences("sparrow.example.com.veryfirst", Context.MODE_PRIVATE);
        editor = sharedPreferences.edit();
        initGame(findViewById(R.id.textView2));
    }

    public void initGame(View view) {
        Random random = new Random();
        correctNumber = random.nextInt((100-0)+1)+0;
        counter = 0;
        TextView textView = (TextView)findViewById(R.id.textView2);
        textView.setText(getString(R.string.timesTried)+counter);
        TextView textView2 = (TextView)findViewById(R.id.textView3);
        textView2.setText(getString(R.string.bestScore)+sharedPreferences.getInt("bestCounter",100));
        Button play = (Button)findViewById(R.id.button);
        play.setVisibility(View.VISIBLE);
        EditText insert = (EditText)findViewById(R.id.editText);
        insert.setVisibility(View.VISIBLE);
    }

    public void alert(String message){
        Context context = getApplicationContext();
        int duration = Toast.LENGTH_SHORT;
        Toast toast = Toast.makeText(context, message, duration);
        toast.show();
        TextView textView = (TextView)findViewById(R.id.textView2);
        textView.setText(getString(R.string.timesTried)+counter);
    }

    public void guess(View view) {
        EditText editText = (EditText)findViewById(R.id.editText);
        String message;
        if( editText.getText().toString().trim().equals("") ) message = getString(R.string.emptyField);
        else
        {
            int userNumber = Integer.parseInt(editText.getText().toString());
            counter++;
            if (correctNumber > userNumber) {
                message = getString(R.string.tooSmall);
            }
            else if (correctNumber < userNumber) {
                message = getString(R.string.tooLarge);
            }
            else {
                message = getString(R.string.correct);
                Button play = (Button)findViewById(R.id.button);
                play.setVisibility(View.GONE);
                EditText insert = (EditText)findViewById(R.id.editText);
                insert.setVisibility(View.GONE);
                if (counter < sharedPreferences.getInt("bestCounter", 100)) {
                    editor.putInt("bestCounter", counter);
                    editor.commit();
                    TextView textView2 = (TextView) findViewById(R.id.textView3);
                    textView2.setText(getString(R.string.bestScore) + sharedPreferences.getInt("bestCounter", 100));
                }
            }
        }
        alert(message);
    }

}
