package com.example.sparrow.notebook;

import android.content.Intent;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;

public class NoteDetailActivity extends AppCompatActivity {
    public static final String NEW_NOTE_KEY = "New Note";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_note_detail);

        createAndAddFragment();
    }

    private void createAndAddFragment(){
        Intent intent = getIntent();
        MainActivity.FragmentToLaunch fragmentToLaunch = (MainActivity.FragmentToLaunch) intent.getSerializableExtra(MainActivity.NOTE_FRAGMENT_MODE_KEY);

        FragmentManager fragmentManager = getSupportFragmentManager();
        FragmentTransaction fragmentTransaction = fragmentManager.beginTransaction();

        switch (fragmentToLaunch){
            case DISPLAY:
                NoteViewFragment noteViewFragment = new NoteViewFragment();
                setTitle(R.string.vieved_fragment_title);
                fragmentTransaction.add(R.id.notesContainer, noteViewFragment, "Note_View_Fragment");
                break;

            case EDIT:
                NoteEditFragment noteEditFragment = new NoteEditFragment();
                setTitle(R.string.edit_note_title);
                fragmentTransaction.add(R.id.notesContainer, noteEditFragment, "Note_Edit_Fragment");
                break;

            case CREATE:
                NoteEditFragment noteCreateFragment = new NoteEditFragment();
                setTitle(getString(R.string.create_note_case));
                Bundle bundle = new Bundle();
                bundle.putBoolean(NEW_NOTE_KEY, true);
                noteCreateFragment.setArguments(bundle);
                fragmentTransaction.add(R.id.notesContainer, noteCreateFragment, "Note_Create_Fragment");
                break;
        }

        //NoteViewFragment noteViewFragment = new NoteViewFragment();
        //setTitle(R.string.vieved_fragment_title);
        //fragmentTransaction.add(R.id.notesContainer, noteViewFragment, "NOTE_VIEW_FRAGMENT");
        fragmentTransaction.commit();
    }
}
