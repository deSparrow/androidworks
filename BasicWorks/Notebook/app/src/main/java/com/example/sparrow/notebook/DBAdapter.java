package com.example.sparrow.notebook;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.SQLException;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;

import java.util.ArrayList;
import java.util.Calendar;

/**
 * Created by sparrow on 03.09.16.
 */
public class DBAdapter {
    private static final String DB_NAME = "Notebook.db";
    private static final int DB_VERSION = 1;
    public static final String NOTE_TABLE = "note";

    public static final String COLUMN_ID = "_id";
    public static final String COLUMN_NAME = "name";
    public static final String COLUMN_MESSAGE = "message";
    public static final String COLUMN_CATEGORY = "category";
    public static final String COLUMN_DATE = "date";

    private String[] columns = {COLUMN_NAME, COLUMN_MESSAGE, COLUMN_ID, COLUMN_DATE, COLUMN_CATEGORY};

    public static final String CREATE_NOTE_TABLE =
            "CREATE TABLE " + NOTE_TABLE + " (" +
                    COLUMN_NAME + " TEXT NOT NULL, " +
                    COLUMN_MESSAGE + " TEXT NOT NULL, " +
                    COLUMN_ID + " INTEGER PRIMARY KEY AUTOINCREMENT, " +
                    COLUMN_DATE + ", " +
                    COLUMN_CATEGORY + " TEXT NOT NULL);";

    private SQLiteDatabase database;
    private Context context;
    private DBHelper dbHelper;

    private static class DBHelper extends SQLiteOpenHelper{

        public DBHelper(Context context) {
            super(context, DB_NAME, null, DB_VERSION);
        }

        @Override
        public void onCreate(SQLiteDatabase sqLiteDatabase) {
            sqLiteDatabase.execSQL(CREATE_NOTE_TABLE);
        }

        @Override
        public void onUpgrade(SQLiteDatabase sqLiteDatabase, int oldVersion, int newVersion) {
            sqLiteDatabase.execSQL("DROP TABLE IF EXISTS " + NOTE_TABLE);
            onCreate(sqLiteDatabase);
        }
    }

    public DBAdapter(Context context){
        this.context = context;
    }

    public DBAdapter open() throws android.database.SQLException{
        dbHelper = new DBHelper(context);
        database = dbHelper.getWritableDatabase();
        return this;
    }

    public void close(){
        dbHelper.close();
    }

    public Note createNote(String title, String message, Note.Category category){
        ContentValues values = new ContentValues();
        values.put(COLUMN_NAME, title);
        values.put(COLUMN_MESSAGE, message);
        values.put(COLUMN_DATE, Calendar.getInstance().getTimeInMillis() + "");
        values.put(COLUMN_CATEGORY, category.name());
        long insertId = database.insert(NOTE_TABLE, null, values);
        Cursor cursor = database.query(NOTE_TABLE, columns, COLUMN_ID + " = " + insertId, null, null, null, null);
        cursor.moveToFirst();
        Note note = cursorToNote(cursor);
        cursor.close();
        return note;
    }

    public long deleteNote(long idDeleteNote){
        return database.delete(NOTE_TABLE, COLUMN_ID + " = " + idDeleteNote, null);
    }

    public long updateNote(long idUpdatedNote, String newName, String newMessage, Note.Category newCategory){
        ContentValues values = new ContentValues();
        values.put(COLUMN_NAME, newName);
        values.put(COLUMN_MESSAGE, newMessage);
        values.put(COLUMN_DATE, Calendar.getInstance().getTimeInMillis() + "");
        values.put(COLUMN_CATEGORY, newCategory.name());
        return database.update(NOTE_TABLE, values, COLUMN_ID + " = " + idUpdatedNote, null);
    }

    public ArrayList<Note> getAllNotes(){
        ArrayList<Note> notes = new ArrayList<Note>();
        Cursor cursor = database.query(NOTE_TABLE, columns, null, null, null, null, null);
        for(cursor.moveToLast(); !cursor.isBeforeFirst(); cursor.moveToPrevious()){
            Note note = cursorToNote(cursor);
            notes.add(note);
        }
        cursor.close();
        return notes;
    }

    private Note cursorToNote(Cursor cursor){
        return new Note(cursor.getString(0), cursor.getString(1), cursor.getLong(2), cursor.getLong(3), Note.Category.valueOf(cursor.getString(4)));
    }
}
