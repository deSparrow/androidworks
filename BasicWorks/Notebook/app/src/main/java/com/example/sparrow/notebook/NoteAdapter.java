package com.example.sparrow.notebook;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import java.util.ArrayList;

/**
 * Created by sparrow on 08.08.16.
 */
public class NoteAdapter extends ArrayAdapter<Note> {
    public NoteAdapter(Context context, ArrayList<Note> notes){
        super(context, 0, notes);
    }

    public static class ViewHolder{
        TextView noteTitle;
        TextView noteBody;
        ImageView noteImage;
    }

    @Override
    public View getView(int position, View converView, ViewGroup parent){
        Note note = getItem(position);
        ViewHolder viewHolder;

        if(converView == null)
        {
            converView = LayoutInflater.from(getContext()).inflate(R.layout.list_row, parent, false);

            viewHolder = new ViewHolder();
            viewHolder.noteTitle = (TextView)converView.findViewById(R.id.listItemNoteTitle);
            viewHolder.noteBody = (TextView)converView.findViewById(R.id.listItemNoteBody);
            viewHolder.noteImage = (ImageView)converView.findViewById(R.id.listItemNoteImage);
            converView.setTag(viewHolder);
        } else {
            viewHolder = (ViewHolder)converView.getTag();
        }

        viewHolder.noteTitle.setText(note.getTitle());
        viewHolder.noteBody.setText(note.getMessage());
        viewHolder.noteImage.setImageResource(note.getAssocDrawable());

        return converView;
    }

}
