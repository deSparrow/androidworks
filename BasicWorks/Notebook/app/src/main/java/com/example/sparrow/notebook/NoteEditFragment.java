package com.example.sparrow.notebook;


import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v7.app.AlertDialog;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageButton;


/**
 * A simple {@link Fragment} subclass.
 */
public class NoteEditFragment extends Fragment {
    private ImageButton noteButton;
    private Note.Category buttonCategory;
    private AlertDialog categoryDialog, confirmDialog;
    private long noteId = 0;
    private static final String modCat = "Modified Category";
    EditText title;
    EditText message;
    private boolean newNote = false;


    public NoteEditFragment() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {

        Bundle bundle = this.getArguments();
        if(bundle != null){
            newNote = bundle.getBoolean(NoteDetailActivity.NEW_NOTE_KEY, false);
        }

        //inflate fragment edit layout
        View fragmentLayout = inflater.inflate(R.layout.fragment_note_edit, container, false);

        //grab widget references from layout
        title = (EditText)fragmentLayout.findViewById(R.id.editNoteTitle);
        message = (EditText)fragmentLayout.findViewById(R.id.editNoteMessage);
        noteButton = (ImageButton)fragmentLayout.findViewById(R.id.editNoteButton);
        Button saveButton = (Button)fragmentLayout.findViewById(R.id.saveNote);

        //populate widgets with note data
        Intent intent = getActivity().getIntent();
        title.setText(intent.getExtras().getString(MainActivity.NOTE_TITLE_KEY, ""));
        message.setText(intent.getExtras().getString(MainActivity.NOTE_MESSAGE_KEY, ""));
        noteId = intent.getExtras().getLong(MainActivity.NOTE_ID_KEY, 0);

        //if we grabbed the cat from a bundle, then we know we changed the orientation and saved information so set our img button background to that category
        if(buttonCategory != null){
            noteButton.setImageResource(Note.categoryDrawable(buttonCategory));
        }
        else if (!newNote)
        {
            Note.Category noteCategory = (Note.Category) intent.getSerializableExtra(MainActivity.NOTE_CATEGORY_KEY);
            buttonCategory = noteCategory;
            noteButton.setImageResource(Note.categoryDrawable(noteCategory));
        }

        if(savedInstanceState != null){
            buttonCategory = (Note.Category) savedInstanceState.get(modCat);
        }

        buildCategoryDialog();
        confirmDialog();

        noteButton.setOnClickListener(new View.OnClickListener(){
            @Override
            public void onClick(View v){
                categoryDialog.show();
            }
        });

        saveButton.setOnClickListener(new View.OnClickListener(){
            @Override
            public void onClick(View v){
                confirmDialog.show();
            }
        });

        // Inflate the layout for this fragment
        return fragmentLayout;
    }

    @Override
    public void onSaveInstanceState(Bundle outState) {
        super.onSaveInstanceState(outState);
        outState.putSerializable(modCat, buttonCategory);
    }

    private void buildCategoryDialog(){
        final String [] categories = new String []{"1.Trifle", "2.Normal", "3.Important", "4.Very important"};
        final AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
        builder.setTitle("Chose priority of the note.");

        builder.setSingleChoiceItems(categories, 0, new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialogInterface, int item) {
                categoryDialog.cancel();

                switch (item){
                    case 0:
                        buttonCategory = Note.Category.GREEN;
                        noteButton.setImageResource(R.drawable.green);
                        break;

                    case 1:
                        buttonCategory = Note.Category.YELLOW;
                        noteButton.setImageResource(R.drawable.yellow);
                        break;

                    case 2:
                        buttonCategory = Note.Category.ORANGE;
                        noteButton.setImageResource(R.drawable.orange);
                        break;

                    case 3:
                        buttonCategory = Note.Category.RED;
                        noteButton.setImageResource(R.drawable.red);
                        break;
                }
            }
        });
        categoryDialog = builder.create();
    }

    private void confirmDialog(){
        AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
        builder.setTitle("Are you sure?");
        builder.setMessage("Are you sure you want to save the note?");
        builder.setPositiveButton("Yes", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialogInterface, int i) {
                Log.d("Save note", "Note title: "+title.getText()+", Note message: "+message.getText()+" Note category: "+buttonCategory);
                DBAdapter dbAdapter = new DBAdapter(getActivity().getBaseContext());
                dbAdapter.open();
                if(newNote){
                    dbAdapter.createNote(title.getText().toString(), message.getText().toString(), (buttonCategory==null)? Note.Category.YELLOW : buttonCategory);
                }
                else {
                    dbAdapter.updateNote(noteId, title.getText().toString(), message.getText().toString(), buttonCategory);
                    System.out.println("Edited **************************************************************");
                }
                dbAdapter.close();
                startActivity(new Intent(getActivity(), MainActivity.class));
            }
        });

        builder.setNegativeButton("Cancel", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialogInterface, int i) {}
        });

        confirmDialog = builder.create();
    }
}
