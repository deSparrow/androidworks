package com.example.sparrow.notebook;


import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.ListFragment;
import android.support.v4.content.ContextCompat;
import android.util.Log;
import android.view.ContextMenu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.ListView;

import java.util.ArrayList;

/**
 * A simple {@link Fragment} subclass.
 */
public class MainActivityListFragment extends ListFragment {
    private ArrayList<Note> notes;
    private NoteAdapter noteAdapter;

    @Override
    public void onActivityCreated(Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);

        /*
        String[] values = new String[]{"Witcher", "Dark Souls", "Alan Wake", "Days gone", "Heroes III"};
        ArrayAdapter<String> adapter = new ArrayAdapter<String>(getActivity(), android.R.layout.simple_list_item_1, values);
        setListAdapter(adapter);
        */
        DBAdapter dbAdapter = new DBAdapter(getActivity().getBaseContext());
        dbAdapter.open();
        notes = dbAdapter.getAllNotes();
        dbAdapter.close();

        noteAdapter = new NoteAdapter(getActivity(), notes);
        setListAdapter(noteAdapter);

        registerForContextMenu(getListView());
    }

    @Override
    public void onListItemClick(ListView l, View v, int position, long id) {
        super.onListItemClick(l, v, position, id);

        launchNoteDetailActivity(MainActivity.FragmentToLaunch.DISPLAY, position);
    }

    @Override
    public boolean onContextItemSelected(MenuItem item) {
        //give me position of note pressed
        AdapterView.AdapterContextMenuInfo adapterContextMenuInfo = (AdapterView.AdapterContextMenuInfo) item.getMenuInfo();
        int position = adapterContextMenuInfo.position;
        Note note = (Note)getListAdapter().getItem(position);

        switch (item.getItemId()){
            case R.id.edit:
                Log.d("Main act list frag", "item id edit selected");
                launchNoteDetailActivity(MainActivity.FragmentToLaunch.EDIT, position);
                return true;

            case R.id.delete:
                DBAdapter dbAdapter = new DBAdapter(getActivity().getBaseContext());
                dbAdapter.open();
                dbAdapter.deleteNote(note.getNoteId());
                notes.clear();
                notes.addAll(dbAdapter.getAllNotes());
                noteAdapter.notifyDataSetChanged();
                dbAdapter.close();
                return true;
        }

        return super.onContextItemSelected(item);
    }

    @Override
    public void onCreateContextMenu(ContextMenu menu, View v, ContextMenu.ContextMenuInfo menuInfo) {
        super.onCreateContextMenu(menu, v, menuInfo);

        MenuInflater menuInflater = getActivity().getMenuInflater();
        menuInflater.inflate(R.menu.long_press_menu, menu);
    }

    private void launchNoteDetailActivity(MainActivity.FragmentToLaunch fragmentToLaunch, int position){
        Note note = (Note)getListAdapter().getItem(position);
        Intent intent = new Intent(getActivity(), NoteDetailActivity.class);
        intent.putExtra(MainActivity.NOTE_ID_KEY, note.getNoteId());
        intent.putExtra(MainActivity.NOTE_TITLE_KEY, note.getTitle());
        intent.putExtra(MainActivity.NOTE_MESSAGE_KEY, note.getMessage());
        intent.putExtra(MainActivity.NOTE_CATEGORY_KEY, note.getCategory());

        switch (fragmentToLaunch){
            case DISPLAY:
                intent.putExtra(MainActivity.NOTE_FRAGMENT_MODE_KEY, MainActivity.FragmentToLaunch.DISPLAY);
                break;

            case EDIT:
                intent.putExtra(MainActivity.NOTE_FRAGMENT_MODE_KEY, MainActivity.FragmentToLaunch.EDIT);
                break;
        }

        startActivity(intent);
    }
}
