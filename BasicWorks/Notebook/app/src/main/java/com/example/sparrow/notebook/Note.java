package com.example.sparrow.notebook;

/**
 * Created by sparrow on 08.08.16.
 */
public class Note {
    private String title, message;
    private long noteId, dateCreatedMilli;
    private Category category;

    public enum Category { GREEN, YELLOW, ORANGE, RED }

    public Note(String title, String message, Category category) {
        this.title = title;
        this.message = message;
        this.category = category;
    }

    public Note(String title, String message, long noteId, long dateCreatedMilli, Category category) {
        this.title = title;
        this.message = message;
        this.noteId = noteId;
        this.dateCreatedMilli = dateCreatedMilli;
        this.category = category;
    }

    public String getTitle() {
        return title;
    }

    public String getMessage() {
        return message;
    }

    public long getNoteId() {
        return noteId;
    }

    public long getDateCreatedMilli() {
        return dateCreatedMilli;
    }

    public Category getCategory() {
        return category;
    }

    @Override
    public String toString() {
        return "Note{" +
                "title='" + title +
                ", message='" + message +
                ", noteId=" + noteId +
                ", dateCreatedMilli=" + dateCreatedMilli +
                ", category=" + category.name() +
                '}';
    }

    public int getAssocDrawable(){
        return categoryDrawable(category);
    }

    public static int categoryDrawable(Category noteCategory){
        switch (noteCategory){
            case GREEN:
                return R.drawable.green;

            case ORANGE:
                return R.drawable.orange;

            case RED:
                return R.drawable.red;

            default:
                return R.drawable.yellow;
        }
    }
}
