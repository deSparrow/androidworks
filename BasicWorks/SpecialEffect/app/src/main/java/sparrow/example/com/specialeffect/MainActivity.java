package sparrow.example.com.specialeffect;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.ImageView;

public class MainActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
    }


    public void rotate(View view) {
        Animation animation = AnimationUtils.loadAnimation(this, R.anim.rotation);
        ImageView imageView = (ImageView)findViewById(R.id.imageView);
        imageView.startAnimation(animation);
    }
}
