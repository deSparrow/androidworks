package sparrow.example.com.tictactoe;

import android.graphics.Color;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;

public class MainActivity extends AppCompatActivity {
    private boolean playerXTourn = true;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
    }

    public void tttButtonClicked(View v){
        Button button = (Button)v;

        if(button.getText().equals("")){
            if(playerXTourn){
                button.setText("X");
                button.setTextColor(Color.RED);
            }
            else{
                button.setText("O");
                button.setTextColor(Color.BLUE);
            }
        }
        playerXTourn = !playerXTourn;
    }

    public void exitApplication(View view) {
        finish();
    }
}
