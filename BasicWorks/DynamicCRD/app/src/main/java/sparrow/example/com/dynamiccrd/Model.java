package sparrow.example.com.dynamiccrd;

/**
 * Created by Sparrow on 22.07.16.
 */
public class Model {
    private String name;

    public Model(String name) {
        this.name = name;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }
}
