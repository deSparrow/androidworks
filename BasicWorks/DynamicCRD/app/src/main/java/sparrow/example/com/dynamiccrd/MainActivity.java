package sparrow.example.com.dynamiccrd;

import java.util.ArrayList;
import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.EditText;
import android.widget.ListView;
import android.widget.Toast;

public class MainActivity extends Activity {

    ListView lv;
    EditText et;

    ArrayList<Model> modelList;
    CustomAdapter adapter;

    Context context;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        lv = (ListView) findViewById(R.id.lv);
        et = (EditText) findViewById(R.id.et);

        context = getApplicationContext();

        modelList = new ArrayList<Model>();
        adapter = new CustomAdapter(context, modelList);
        lv.setAdapter(adapter);

        Model md = new Model("Is it?");
        modelList.add(md);
        adapter.notifyDataSetChanged();
        et.setText("");
    }

    public void addbtn(View v) {
        String name = et.getText().toString();

        if (name.isEmpty()) {
            Toast.makeText(getApplicationContext(), "Plz enter Values",
                    Toast.LENGTH_SHORT).show();
        } else {
            Model md = new Model(name);
            modelList.add(md);
            adapter.notifyDataSetChanged();
            et.setText("");
        }

    }

    public void invokeDrawer(View view) {
        Intent intent = new Intent(context, DrawerActivity.class);
        intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
        context.startActivity(intent);
    }
}