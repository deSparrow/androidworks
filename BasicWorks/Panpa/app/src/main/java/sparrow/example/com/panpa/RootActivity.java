package sparrow.example.com.panpa;

import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;

/**
 * Created by Sparrow on 04.07.16.
 */
public class RootActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        setContentView(R.layout.activity_root);
    }

}
