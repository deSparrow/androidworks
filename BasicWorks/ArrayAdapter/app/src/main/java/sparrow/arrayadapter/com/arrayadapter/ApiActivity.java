package sparrow.arrayadapter.com.arrayadapter;

import android.app.ProgressDialog;
import android.content.Intent;
import android.preference.EditTextPreference;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.EditText;
import android.widget.ListView;
import android.widget.ProgressBar;
import android.widget.Toast;
import com.loopj.android.http.*;

import org.json.JSONObject;

import java.io.UnsupportedEncodingException;
import java.net.URLEncoder;

public class ApiActivity extends AppCompatActivity {
    private static final String QUERY_URL = "http://openlibrary.org/search.json?q=";
    private EditText editText;
    JsonAdapter jsonAdapter;
    private ListView mainListView;
    private ProgressDialog progressDialog;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_api);

        editText = (EditText)findViewById(R.id.editText);

        // 10. Create a JSONAdapter for the ListView
        jsonAdapter = new JsonAdapter(this, getLayoutInflater());

        // Set the ListView to use the ArrayAdapter
        mainListView = (ListView) findViewById( R.id.list_from_api);
        mainListView.setAdapter(jsonAdapter);
        mainListView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> adapterView, View view, int position, long l) {
                // 12. Now that the user's chosen a book, grab the cover data
                JSONObject jsonObject = (JSONObject) jsonAdapter.getItem(position);
                String coverID = jsonObject.optString("cover_i","");

                // create an Intent to take you over to a new DetailActivity
                Intent detailIntent = new Intent(getApplicationContext(), DetailActivity.class);

                // pack away the data about the cover
                // into your Intent before you head out
                detailIntent.putExtra("coverID", coverID);

                startActivity(detailIntent);
            }
        });

        progressDialog = new ProgressDialog(this);
        progressDialog.setMessage("Searching for books");
        progressDialog.setCancelable(false);
    }

    private void queryBooks(String searchString) {

        // Prepare your search string to be put in a URL
        // It might have reserved characters or something
        String urlString = "";
        try {
            urlString = URLEncoder.encode(searchString, "UTF-8");
        } catch (UnsupportedEncodingException e) {

            // if this fails for some reason, let the user know why
            e.printStackTrace();
            Toast.makeText(this, "Error: " + e.getMessage(), Toast.LENGTH_LONG).show();
        }

        // Create a client to perform networking
        AsyncHttpClient client = new AsyncHttpClient();
        progressDialog.show();

        // Have the client get a JSONArray of data
        // and define how to respond
        client.get(QUERY_URL + urlString,
                new JsonHttpResponseHandler() {

                    @Override
                    public void onSuccess(JSONObject jsonObject) {
                        progressDialog.dismiss();
                        // Display a "Toast" message
                        // to announce your success
                        Toast.makeText(getApplicationContext(), "Success!", Toast.LENGTH_LONG).show();

                        // 8. For now, just log results
                        Log.d("omg android", jsonObject.toString());
                        // update the data in your custom method.
                        jsonAdapter.updateData(jsonObject.optJSONArray("docs"));
                    }

                    @Override
                    public void onFailure(int statusCode, Throwable throwable, JSONObject error) {
                        progressDialog.dismiss();
                        // Display a "Toast" message
                        // to announce the failure
                        Toast.makeText(getApplicationContext(), "Error: " + statusCode + " " + throwable.getMessage(), Toast.LENGTH_LONG).show();

                        // Log error message
                        // to help solve any problems
                        Log.e("omg android", statusCode + " " + throwable.getMessage());
                    }
                });
    }

    public void click(View view) {
        // 9. Take what was typed into the EditText and use in search
        queryBooks(editText.getText().toString());
    }
}
