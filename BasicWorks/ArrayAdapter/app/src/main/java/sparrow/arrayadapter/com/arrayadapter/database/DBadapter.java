package sparrow.arrayadapter.com.arrayadapter.database;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.SQLException;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteException;
import android.database.sqlite.SQLiteOpenHelper;
import android.util.Log;

import sparrow.arrayadapter.com.arrayadapter.Product;

/**
 * Created by sparrow on 09.12.16.
 */

public class DBadapter {
    private static final String DB_PRODUCTS_TABLE = "Products";
    private static final int DB_VERSION = 1;
    private static final String DB_NAME = "database.db";
    private SQLiteDatabase database;
    private Context context;
    private DBhelper dbHelper;

    public static final String KEY_ID = "Id";
    public static final int ID_COLUMN = 0;
    public static final String ID_OPTIONS = "INTEGER PRIMARY KEY AUTOINCREMENT";

    public static final String KEY_NAME = "Name";
    public static final int NAME_COLUMN = 1;
    public static final String NAME_OPTIONS = "TEXT DEFAULT ''";

    public static final String KEY_IMAGE_ID = "ImageID";
    public static final int IMAGE_ID_COLUMN = 2;
    public static final String IMAGE_ID_OPTIONS = "INTEGER DEFAULT 1";

    private static final String DB_CREATE_PRODUCTS_TABLE =
            "CREATE TABLE " + DB_PRODUCTS_TABLE + "( " +
                    KEY_ID + " " + ID_OPTIONS + ", " + KEY_NAME + " " + NAME_OPTIONS + ", "+ KEY_IMAGE_ID + " " + IMAGE_ID_OPTIONS + ");";

    private static final String DB_DROP_PRODUCTS_TABLE = "DROP TABLE IF EXISTS " + DB_PRODUCTS_TABLE;

    public DBadapter(Context context){
        this.context = context;
    }

    public class DBhelper extends SQLiteOpenHelper{

        public DBhelper(Context context, String name, SQLiteDatabase.CursorFactory factory, int version) {
            super(context, name, factory, version);
        }

        @Override
        public void onCreate(SQLiteDatabase db) {
            try{
                Log.d(getClass().getName(), "Creating database.");
                db.execSQL(DB_CREATE_PRODUCTS_TABLE);
                Log.d(getClass().getName(), "Database created.");
            }
            catch (SQLiteException e){
                Log.d(getClass().getName(), "Failed to create database.");
            }
        }

        @Override
        public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
            try{
                Log.d(getClass().getName(), "Updating database.");
                db.execSQL(DB_DROP_PRODUCTS_TABLE);
                Log.d(getClass().getName(), "Database updated from " + oldVersion + " to " + newVersion + ", all data are lost.");
                onCreate(db);
            }
            catch (SQLiteException e){
                Log.d(getClass().getName(), "Failed to update database.");
            }
        }
    }

    public DBadapter open(){
        Log.d(getClass().getName(), "Opening database..");
        dbHelper = new DBhelper(context, DB_NAME, null, DB_VERSION);
        try{
            database = dbHelper.getWritableDatabase();
            Log.d(getClass().getName(), "Database opened.");
        } catch (SQLException e){
            database = dbHelper.getReadableDatabase();
            Log.d(getClass().getName(), "Database failed to open in writable mode, trying to access readable mode at least.");
        }
        return this;
    }

    public void close(){
        dbHelper.close();
    }

    public long insertProduct(String name, int imageID){
        ContentValues contentValues = new ContentValues();
        contentValues.put(KEY_NAME, name);
        contentValues.put(KEY_IMAGE_ID, imageID);
        return database.insert(DB_PRODUCTS_TABLE, null, contentValues);
    }

    public boolean updateProduct(Product product){
        long id = product.getId();
        String name = product.getProductName();
        int imageId = product.getImageId();

        return updateProduct(id, name, imageId);
    }

    public boolean updateProduct(long id, String name, int imageId){
        String where = KEY_ID + "=" + id;
        ContentValues contentValues = new ContentValues();
        contentValues.put(KEY_NAME, name);
        contentValues.put(KEY_IMAGE_ID, imageId);

        return database.update(DB_PRODUCTS_TABLE, contentValues, where, null) > 0;
    }

    public boolean deleteProduct(long id){
        String where = KEY_ID + "=" + id;
        return database.delete(DB_PRODUCTS_TABLE, where, null) > 0;
    }

    public boolean deleteAllProducts(){
        return database.delete(DB_PRODUCTS_TABLE, null, null) > 0;
    }

    public Product getProduct(long id){
        String[] columns = {KEY_ID, KEY_NAME, KEY_IMAGE_ID};
        String where = KEY_ID + "=" + id;
        Cursor cursor = database.query(DB_PRODUCTS_TABLE, columns, where, null, null, null, null);
        Product product = null;
        if(cursor != null && cursor.moveToFirst()){
            String name = cursor.getString(NAME_COLUMN);
            int imageID = cursor.getInt(IMAGE_ID_COLUMN);

            product = new Product(id, name, imageID);
        }
        return product;
    }

    public Cursor getAllProducts(){
        String[] columns = {KEY_ID, KEY_NAME, KEY_IMAGE_ID};
        return database.query(DB_PRODUCTS_TABLE, columns, null, null, null, null, null);
    }
}
