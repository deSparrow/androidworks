package sparrow.arrayadapter.com.arrayadapter;

import android.app.Dialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.database.Cursor;
import android.database.sqlite.SQLiteException;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.text.InputType;
import android.view.View;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ListView;
import android.widget.RadioGroup;
import android.widget.Toast;

import java.util.ArrayList;
import java.util.List;

import sparrow.arrayadapter.com.arrayadapter.database.DBadapter;

public class MainActivity extends AppCompatActivity {
    private List products;
    private ListView lvProducts;
    private Button button;
    private DBadapter databaseAdapter;
    private Cursor cursor;
    private ProductAdapter productAdapter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        button = (Button)findViewById(R.id.button);
        products = new ArrayList();

        databaseAdapter = new DBadapter(this);
        databaseAdapter.open();
        cursor = databaseAdapter.getAllProducts();
        cursor.moveToFirst();
        while(!cursor.isAfterLast()){
            Product product = databaseAdapter.getProduct(cursor.getLong(DBadapter.ID_COLUMN));
            products.add(product);
            cursor.moveToNext();
        }
        lvProducts = (ListView) findViewById( R.id.list_products);
        productAdapter = new ProductAdapter(this, products);
        lvProducts.setAdapter(productAdapter);
        lvProducts.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> adapterView, final View view, final int i, long l) {
                new AlertDialog.Builder(view.getContext())
                        .setTitle("Delete product")
                        .setMessage("Are you sure you want to delete this product?")
                        .setPositiveButton("Yes", new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dialog, int which) {
                                databaseAdapter.open();
                                Product p = (Product) products.get(i);
                                databaseAdapter.deleteProduct(p.getId());
                                databaseAdapter.close();
                                products.remove(i);
                                productAdapter.notifyDataSetChanged();
                                Toast.makeText(view.getContext(), "Product deleted successfully", Toast.LENGTH_SHORT).show();
                            }
                        })
                        .setNegativeButton("No", new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dialog, int which) {
                                dialog.dismiss();
                            }
                        })
                        .setNeutralButton("Edit", new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialogInterface, int which) {


                                final Dialog dialog = new Dialog(view.getContext());
                                dialog.setContentView(R.layout.radio_buttons_in_alert);
                                dialog.setTitle("Edit");
                                dialog.setCancelable(true);
                                final RadioGroup radioGroup = (RadioGroup)dialog.findViewById(R.id.radio_group);
                                final EditText input = (EditText) dialog.findViewById(R.id.dialog_text);
                                final Product p = (Product) products.get(i);
                                input.setText(p.getProductName());
                                radioGroup.check(R.id.radio_button_first);
                                Button button = (Button)dialog.findViewById(R.id.dialog_button);
                                button.setOnClickListener(new View.OnClickListener() {
                                    @Override
                                    public void onClick(View view) {
                                        try {
                                            databaseAdapter.open();
                                            int imageID;
                                            switch (radioGroup.getCheckedRadioButtonId()){
                                                case R.id.radio_button_first:
                                                    imageID = R.drawable.airline_seat;
                                                    break;

                                                case R.id.radio_button_second:
                                                    imageID = R.drawable.eye;
                                                    break;

                                                default:
                                                    imageID = R.drawable.graphic;
                                            }
                                            p.setImageId(imageID);
                                            p.setProductName(input.getText().toString());
                                            databaseAdapter.updateProduct(p);
                                            productAdapter.notifyDataSetChanged();
                                            databaseAdapter.close();
                                        } catch (SQLiteException e){
                                            Toast.makeText(view.getContext(), "Failed to edit product", Toast.LENGTH_SHORT).show();
                                            return;
                                        }
                                        dialog.dismiss();

                                    }
                                });

                                dialog.show();



                            }
                        })
                        .setIcon(android.R.drawable.ic_dialog_alert)
                        .show();
            }
        });

        databaseAdapter.close();

//        databaseAdapter.open();
//        databaseAdapter.deleteAllProducts();
//        databaseAdapter.close();
    
    }

    public void click(View view) {
        final Dialog dialog = new Dialog(this);
        dialog.setContentView(R.layout.radio_buttons_in_alert);
        dialog.setTitle("New product");
        dialog.setCancelable(true);
        final RadioGroup radioGroup = (RadioGroup)dialog.findViewById(R.id.radio_group);
        final EditText input = (EditText) dialog.findViewById(R.id.dialog_text);
        radioGroup.check(R.id.radio_button_first);
        Button button = (Button)dialog.findViewById(R.id.dialog_button);
        button.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                try {
                    databaseAdapter.open();
                    int imageID;
                    switch (radioGroup.getCheckedRadioButtonId()){
                        case R.id.radio_button_first:
                            imageID = R.drawable.airline_seat;
                            break;

                        case R.id.radio_button_second:
                            imageID = R.drawable.eye;
                            break;

                        default:
                            imageID = R.drawable.graphic;
                    }
                    long id = databaseAdapter.insertProduct(input.getText().toString(), imageID);
                    products.add(databaseAdapter.getProduct(id));
                    productAdapter.notifyDataSetChanged();
                    databaseAdapter.close();
                } catch (SQLiteException e){
                    Toast.makeText(view.getContext(), "Failed to create product", Toast.LENGTH_SHORT).show();
                    return;
                }
                dialog.dismiss();

            }
        });

        dialog.show();
    }

    public void goToApiActivity(View view) {
        startActivity(new Intent(this, ApiActivity.class));
    }
}
