package sparrow.arrayadapter.com.arrayadapter;

import android.net.Uri;

/**
 * Created by Sparrow on 04.11.16.
 */
public class Product {
    private long id;
    private int imageId;
    private String productName;

    public Product(long id, String productName, int imageId) {
        this.id = id;
        this.imageId = imageId;
        this.productName = productName;
    }

    public Product(int imageId, String productName) {
        this.imageId = imageId;
        this.productName = productName;
    }

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public int getImageId() {
        return imageId;
    }

    public void setImageId(int imageId) {
        this.imageId = imageId;
    }

    public String getProductName() {
        return productName;
    }

    public void setProductName(String productName) {
        this.productName = productName;
    }
}
