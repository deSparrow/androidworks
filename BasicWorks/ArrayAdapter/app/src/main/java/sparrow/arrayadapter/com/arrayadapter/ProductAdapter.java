package sparrow.arrayadapter.com.arrayadapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import java.util.List;

/**
 * Created by Sparrow on 04.11.16.
 */
public class ProductAdapter extends ArrayAdapter<Product> {
    List<Product> mylist;

    public ProductAdapter(Context _context, List<Product> _mylist) {
        super(_context, R.layout.list_item, _mylist);

        this.mylist = _mylist;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        convertView = new LinearLayout(getContext());
        String inflater = Context.LAYOUT_INFLATER_SERVICE;
        LayoutInflater vi = (LayoutInflater)getContext().getSystemService(inflater);
        convertView = vi.inflate(R.layout.list_item, parent, false);

        // Product object
        Product product = getItem(position);

        //
        TextView txtTitle = (TextView) convertView.findViewById(R.id.title);
        txtTitle.setText(product.getProductName());

        // show image
        ImageView img = (ImageView)convertView.findViewById(R.id.image);
        img.setImageResource(product.getImageId());


        return convertView;
    }

}
