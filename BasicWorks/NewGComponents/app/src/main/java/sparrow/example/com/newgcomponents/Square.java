package sparrow.example.com.newgcomponents;

import android.content.Context;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;
import android.util.AttributeSet;
import android.view.View;

/**
 * Created by Sparrow on 10.07.16.
 */
public class Square extends View {

    public Square(Context context) {
        super(context);
    }

    public Square(Context context, AttributeSet attrs) {
        super(context, attrs);
    }

    public Square(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
    }

    @Override
    protected void onDraw(Canvas canvas) {
        int width = getWidth(), height = getHeight();
        int r = 200;

        Paint paint = new Paint();
        paint.setStyle(Paint.Style.FILL);
        paint.setColor(Color.BLUE);
        canvas.drawRect(width/2-r, height/2-r, width/2+r, height/2+r, paint);

        paint.setColor(Color.RED);
        canvas.drawCircle(width/2, height/2, r, paint);

        super.onDraw(canvas);
    }
}
