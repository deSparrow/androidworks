package sparrow.example.com.programmingchapter;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.TextView;

import java.util.Calendar;

public class MainActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
    }

    public void exit(View view) {
        finish();
    }

    public void setDate(View view) {
        int r, m, d, h, min, s;
        Calendar calendar = Calendar.getInstance();
        r = calendar.get(Calendar.YEAR);
        m = calendar.get(Calendar.MONTH);
        d = calendar.get(Calendar.DAY_OF_MONTH);
        h = calendar.get(Calendar.HOUR_OF_DAY);
        min = calendar.get(Calendar.MINUTE);
        s = calendar.get(Calendar.SECOND);

        TextView date = (TextView)findViewById(R.id.textView1);
        date.setText(d+". "+m+". "+r+".");
        TextView time = (TextView)findViewById(R.id.textView2);
        time.setText(h+":"+min+":"+s);
    }
}
