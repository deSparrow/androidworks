# Android Works #
I am deSparrow - polish student in third year of Computer Science at the Bialystok University of Technology.
Welcome to my repository. I mainly make android applications and LibGDX based games on mobile and desktop.
I use this repo to gather all the stuff I make basing on some tutorials or coding all by myself.
Currently, I do my Engineering Thesis (which unfortunately I cannot store here, due to my school's restrictions)
and I look for the opportunity to complete my internship. If you would like to contact me, use one
of the following emails: 

* sparrowhawk1076[at]gmail.com
* work.f.olszewski[at]gmail.com.

Check out some of my works:

## Dexterity ##
	Dexterity is essentially a Flappy Birds look alike game with ports for desktop and Android devices. It has different
	tiles and rewritten mechanics. Also gaps between lightsabers are various. Music is available.

Android device: | Desktop:  
-----| ---- 
![Img1](https://bitbucket.org/repo/oB5Rp4/images/1483923153-Zaznaczenie_036.png) |  ![Img2](https://bitbucket.org/repo/oB5Rp4/images/2911503506-Flappy%20birds_037.png)


## ScannerApp ##
	ScannerApp is an application that uses zxing library to scan UPC code from the products and then through the
	Ebay API it looks for the products with the same UPC and returns the JSON with those products with basic
	details about them. User can place products into the basket. Two languages are available: PL, ENG.


Introducing activity: | Menu activity:  
-----| ---- 
![Img3.png](https://bitbucket.org/repo/oB5Rp4/images/406599498-Img3.png) | ![Img4.png](https://bitbucket.org/repo/oB5Rp4/images/359778021-Img4.png)