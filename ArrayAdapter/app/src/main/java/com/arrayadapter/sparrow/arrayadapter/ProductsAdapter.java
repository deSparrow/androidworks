package com.arrayadapter.sparrow.arrayadapter;

import android.app.Activity;
import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by sparrow on 03.11.16.
 */

public class ProductsAdapter extends ArrayAdapter<Product> {
    Context context;

    public ProductsAdapter(Context context, int resourceId, List<Product> products) {
        super(context, resourceId, products);
        this.context = context;
    }

    private class ViewHolder {
        TextView name;
        ImageView icon;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        ViewHolder holder = null;
        Product product = getItem(position);
        System.out.println(product.getProductName());

        LayoutInflater mInflater = (LayoutInflater) context.getSystemService(Activity.LAYOUT_INFLATER_SERVICE);
        if (convertView == null) {
            convertView = mInflater.inflate(R.layout.product_template, null);
            holder = new ViewHolder();
            holder.name = (TextView) convertView.findViewById(R.id.title);
            holder.icon = (ImageView) convertView.findViewById(R.id.icon);
            convertView.setTag(holder);
        } else {
            holder = (ViewHolder) convertView.getTag();
        }

        holder.name.setText(product.getProductName());
        holder.icon.setImageResource(product.getImageId());

        return convertView;
    }
}
