package com.arrayadapter.sparrow.arrayadapter;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.widget.ListView;

import java.util.ArrayList;
import java.util.List;

public class MainActivity extends AppCompatActivity {

    public static final String[] titles = new String[] { "Strawberry",
            "Banana", "Orange"};

    public static final Integer[] images = { R.drawable.rowing,
            R.drawable.shift_purple, R.drawable.vintage_purple};

    ListView listView;
    List<Product> products;

    /** Called when the activity is first created. */
    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        products = new ArrayList<Product>();
        for (int i = 0; i < titles.length; i++) {
            Product item = new Product(images[i], titles[i]);
            products.add(item);
        }

        listView = (ListView) findViewById(R.id.product_listview);
        ProductsAdapter adapter = new ProductsAdapter(this, R.layout.product_template, products);
        listView.setAdapter(adapter);
    }

/*
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        // Construct the data source
        ArrayList<Product> products = new ArrayList<Product>();

        Product p1, p2, p3;
        p1 = new Product(R.drawable.rowing, "First product");
        p2 = new Product(R.drawable.shift_purple, "Second product");
        p3 = new Product(R.drawable.vintage_purple, "Third product");

        products.add(0, p1);
        products.add(1, p2);
        products.add(2, p3);

        // Create the adapter to convert the array to views
        ProductsAdapter adapter = new ProductsAdapter(this, R.layout.product_template, products);
        // Attach the adapter to a ListView
        ListView listView = (ListView) findViewById(R.id.products_listview);
        listView.setAdapter(adapter);
    }
    */
}
