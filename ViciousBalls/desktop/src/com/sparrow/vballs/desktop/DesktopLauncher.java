package com.sparrow.vballs.desktop;

import com.badlogic.gdx.backends.lwjgl.LwjglApplication;
import com.badlogic.gdx.backends.lwjgl.LwjglApplicationConfiguration;
import com.sparrow.vballs.ViciousBallsGame;
import com.sparrow.vballs.config.GlobalConst;

import java.awt.Dimension;
import java.awt.Toolkit;

public class DesktopLauncher {
	public static void main (String[] arg) {
		LwjglApplicationConfiguration config = new LwjglApplicationConfiguration();
		Dimension screenSize = Toolkit.getDefaultToolkit().getScreenSize();
		config.width = (int)screenSize.getWidth();
		config.height = (int)screenSize.getHeight();
		config.resizable = false;
		new LwjglApplication(new ViciousBallsGame(), config);
	}
}
