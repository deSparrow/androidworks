package com.sparrow.vballs.stages;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.graphics.OrthographicCamera;
import com.badlogic.gdx.math.Rectangle;
import com.badlogic.gdx.math.Vector3;
import com.badlogic.gdx.physics.box2d.Body;
import com.badlogic.gdx.physics.box2d.Box2DDebugRenderer;
import com.badlogic.gdx.physics.box2d.Contact;
import com.badlogic.gdx.physics.box2d.ContactImpulse;
import com.badlogic.gdx.physics.box2d.ContactListener;
import com.badlogic.gdx.physics.box2d.Manifold;
import com.badlogic.gdx.physics.box2d.World;
import com.badlogic.gdx.scenes.scene2d.Stage;
import com.badlogic.gdx.utils.Array;
import com.badlogic.gdx.utils.Scaling;
import com.badlogic.gdx.utils.viewport.ScalingViewport;
import com.sparrow.vballs.actors.Background;
import com.sparrow.vballs.actors.Enemy;
import com.sparrow.vballs.actors.Ground;
import com.sparrow.vballs.actors.Runner;
import com.sparrow.vballs.config.GlobalConst;
import com.sparrow.vballs.config.WorldCreator;
import com.sparrow.vballs.utils.BodyUtils;

/**
 * Created by sparrow on 14.04.17.
 */

public class GameStage extends Stage implements ContactListener {
    private static final int VIEWPORT_WIDTH = GlobalConst.SCREEN_WIDTH;
    private static final int VIEWPORT_HEIGHT = GlobalConst.SCREEN_HEIGHT;
    private World world;
    private Ground ground;
    private Runner runner;
    private OrthographicCamera orthographicCamera;
    private Rectangle screenRightSide;
    private Rectangle screenLeftSide;
    private Vector3 touchPoint;
    private final float TIME_STEP = 1/300f;
    private float time = 0.0f;

    public GameStage() {
        super(new ScalingViewport(Scaling.stretch, VIEWPORT_WIDTH, VIEWPORT_HEIGHT, new OrthographicCamera(VIEWPORT_WIDTH, VIEWPORT_HEIGHT)));
        initWorld();
        initTouchControlAreas();
        initCamera();
    }

    private void initTouchControlAreas() {
        touchPoint = new Vector3();
        screenLeftSide = new Rectangle(0, 0, getCamera().viewportWidth / 2, getCamera().viewportHeight);
        screenRightSide = new Rectangle(getCamera().viewportWidth/2, 0, getCamera().viewportWidth/2, getCamera().viewportHeight);
        Gdx.input.setInputProcessor(this);
    }

    private void initWorld() {
        world = WorldCreator.createWorld();
        world.setContactListener(this);
        initBackground();
        initGround();
        initRunner();
        createEnemy();
    }

    private void createEnemy() {
        Enemy enemy = new Enemy(WorldCreator.createEnemy(world));
        addActor(enemy);
    }

    private void initRunner() {
        runner = new Runner(WorldCreator.createRunner(world));
        addActor(runner);
    }

    private void initGround() {
        ground = new Ground(WorldCreator.createGround(world));
        addActor(ground);
    }

    private void initBackground(){
        addActor(new Background());
    }

    private void initCamera() {
        orthographicCamera = new OrthographicCamera(VIEWPORT_WIDTH, VIEWPORT_HEIGHT);
        orthographicCamera.position.set(orthographicCamera.viewportWidth/2, orthographicCamera.viewportHeight/2, 0.0f);
        orthographicCamera.update();
    }

    private void update(Body body) {
        if (!BodyUtils.bodyInBounds(body)) {
            if (BodyUtils.bodyIsEnemy(body) && !runner.isHit()) {
                createEnemy();
            }
            world.destroyBody(body);
        }
    }



    @Override
    public void draw() {
        super.draw();
    }

    @Override
    public boolean touchDown(int screenX, int screenY, int pointer, int button) {
        translateScreenToWorldCoordinates(screenX, screenY);

        if(rightSideTouched(touchPoint.x, touchPoint.y)){
            runner.jump();
        } else if(leftSideTouched(touchPoint.x, touchPoint.y)){
            runner.dodge();
        }

        return super.touchDown(screenX, screenY, pointer, button);
    }

    private boolean rightSideTouched(float x, float y) {
        return screenRightSide.contains(x, y);
    }

    private boolean leftSideTouched(float x, float y){
        return screenLeftSide.contains(x, y);
    }
    /**
     * Helper function to get the actual coordinates in my world
     * @param x
     * @param y
     */
    private void translateScreenToWorldCoordinates(int x, int y) {
        getCamera().unproject(touchPoint.set(x, y, 0));
    }

    @Override
    public boolean touchUp(int screenX, int screenY, int pointer, int button) {
        if(runner.isDoging()){
            runner.stopDodge();
        }
        return super.touchUp(screenX, screenY, pointer, button);
    }

    @Override
    public void act(float deltaTime) {
        super.act(deltaTime);

        Array<Body> bodies = new Array<Body>(world.getBodyCount());
        world.getBodies(bodies);
        for (Body body : bodies) {
            update(body);
        }

        time += deltaTime;
        while(time>=deltaTime) {
            world.step(TIME_STEP, 6, 2);
            time -= TIME_STEP;
        }
    }

    @Override
    public void beginContact(Contact contact) {
        Body a = contact.getFixtureA().getBody();
        Body b = contact.getFixtureB().getBody();

        if ((BodyUtils.bodyIsRunner(a) && BodyUtils.bodyIsEnemy(b)) ||
                (BodyUtils.bodyIsEnemy(a) && BodyUtils.bodyIsRunner(b))) {
            runner.hit();
        } else if ((BodyUtils.bodyIsRunner(a) && BodyUtils.bodyIsGround(b)) ||
                (BodyUtils.bodyIsGround(a) && BodyUtils.bodyIsRunner(b))) {
            runner.landed();
        }
    }


    @Override
    public void endContact(Contact contact) {

    }

    @Override
    public void preSolve(Contact contact, Manifold oldManifold) {

    }

    @Override
    public void postSolve(Contact contact, ContactImpulse impulse) {

    }
}
