package com.sparrow.vballs.enums;

import com.sparrow.vballs.config.GlobalConst;

/**
 * Created by sparrow on 01.05.17.
 */

public enum EnemyType {

    RUNNING_SMALL(1f, 1f, GlobalConst.ENEMY_X, GlobalConst.RUNNING_SHORT_ENEMY_Y, GlobalConst.ENEMY_DENSITY, GlobalConst.RUNNING_SMALL_ENEMY_REGION_NAMES),
    RUNNING_WIDE(2f, 1f, GlobalConst.ENEMY_X, GlobalConst.RUNNING_SHORT_ENEMY_Y, GlobalConst.ENEMY_DENSITY, GlobalConst.RUNNING_WIDE_ENEMY_REGION_NAMES),
    RUNNING_LONG(1f, 2f, GlobalConst.ENEMY_X, GlobalConst.RUNNING_LONG_ENEMY_Y, GlobalConst.ENEMY_DENSITY, GlobalConst.RUNNING_LONG_ENEMY_REGION_NAMES),
    RUNNING_BIG(2f, 2f, GlobalConst.ENEMY_X, GlobalConst.RUNNING_LONG_ENEMY_Y, GlobalConst.ENEMY_DENSITY, GlobalConst.RUNNING_BIG_ENEMY_REGION_NAMES),
    FLYING_SMALL(1f, 1f, GlobalConst.ENEMY_X, GlobalConst.FLYING_ENEMY_Y, GlobalConst.ENEMY_DENSITY, GlobalConst.RUNNING_SMALL_ENEMY_REGION_NAMES),
    FLYING_WIDE(2f, 1f, GlobalConst.ENEMY_X, GlobalConst.FLYING_ENEMY_Y, GlobalConst.ENEMY_DENSITY, GlobalConst.RUNNING_WIDE_ENEMY_REGION_NAMES);

    private float width;
    private float height;
    private float x;
    private float y;
    private float density;

    private String[] regions;

    EnemyType(float width, float height, float x, float y, float density, String[] regions) {
        this.width = width;
        this.height = height;
        this.x = x;
        this.y = y;
        this.density = density;
        this.regions = regions;
    }

    public float getWidth() {
        return width;
    }

    public float getHeight() {
        return height;
    }

    public float getX() {
        return x;
    }

    public float getY() {
        return y;
    }

    public float getDensity() {
        return density;
    }

    public String[] getRegions() {
        return regions;
    }

}