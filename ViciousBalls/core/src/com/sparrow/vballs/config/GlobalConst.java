package com.sparrow.vballs.config;

import com.badlogic.gdx.math.Vector2;

/**
 * Created by sparrow on 14.04.17.
 */

public class GlobalConst {
    public static final Vector2 WORLD_GRAVITY = new Vector2(0.0f, -10.0f);
    public static final Vector2 RUNNER_JUMPING_LINEAR_IMPULSE = new Vector2(0, 16f);
    public static final Vector2 ENEMY_LINEAR_VELOCITY = new Vector2(-5.28f, 0);
    public static final int SCREEN_WIDTH = 800;
    public static final int SCREEN_HEIGHT = 480;
    public static final float WORLD_TO_SCREEN = 32;
    public static final float GROUND_X = 0f;
    public static final float GROUND_Y = 0f;
    public static final float GROUND_WIDTH = 25f;
    public static final float GROUND_HEIGHT = 2f;
    public static final float GROUND_DENSITY = 0f;
    public static final float RUNNER_X = 2;
    public static final float RUNNER_Y = GROUND_Y + GROUND_HEIGHT;
    public static final float RUNNER_WIDTH = 1f;
    public static final float RUNNER_HEIGHT = 2f;
    public static final float RUNNER_GRAVITY_SCALE = 3f;
    public static float RUNNER_DENSITY = 0.5f;
    public static final float ENEMY_X = 25f;
    public static final float ENEMY_DENSITY = RUNNER_DENSITY;
    public static final float RUNNING_SHORT_ENEMY_Y = 1.5f;
    public static final float RUNNING_LONG_ENEMY_Y = 2f;
    public static final float FLYING_ENEMY_Y = 3f;
    public static final float RUNNER_HIT_ANGULAR_IMPULSE = -4f;
    public static final String BACKGROUND_STATIC_PATH = "backgrounds/bg_cementery_static.png";
    public static final String BACKGROUND_DYNAMIC_PATH = "backgrounds/bg_cementery_dynamic.png";
    public static final String GHOST_ATLAS_PATH = "atlasses/Ghost.txt";
    public static final String GROUND_ENEMIES_1_ATLAS_PATH = "atlasses/Ground_enemies.txt";
    public static final String PLAYER_RUN_ATLAS_PATH = "atlasses/Player_run.txt";
    public static final String PLAYER_JUMP_ATLAS_PATH = "atlasses/Player_jump.txt";
    public static final String PLAYER_DEAD_ATLAS_PATH = "atlasses/Player_dead.txt";
    public static final String PLAYER_SLIDE_ATLAS_PATH = "atlasses/Player_slide.txt";
    public static final String[] PLAYER_RUNNING_REGION_NAMES = new String[] {"Run (1)", "Run (2)", "Run (3)", "Run (4)", "Run (5)", "Run (6)", "Run (7)", "Run (8)"};
    public static final String PLAYER_ROLLING_REGION_NAME = "Slide (1)";
    public static final String PLAYER_HIT_REGION_NAME = "Dead (4)";
    public static final String PLAYER_JUMPING_REGION_NAME = "Jump (5)";
    public static final String[] RUNNING_SMALL_ENEMY_REGION_NAMES = new String[] {"Crate"};
    public static final String[] RUNNING_LONG_ENEMY_REGION_NAMES = new String[] {"TombStone"};
    public static final String[] RUNNING_BIG_ENEMY_REGION_NAMES = new String[] {"Tree"};
    public static final String[] RUNNING_WIDE_ENEMY_REGION_NAMES = new String[] {"Skeleton"};
    public static final String[] FLYING_SMALL_ENEMY_REGION_NAMES = new String[] {"ghost"};
    public static final String[] FLYING_WIDE_ENEMY_REGION_NAMES = new String[] {"ghost_wide (1)", "ghost_wide (2)"};


}
