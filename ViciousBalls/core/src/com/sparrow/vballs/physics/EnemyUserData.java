package com.sparrow.vballs.physics;

import com.badlogic.gdx.math.Vector2;
import com.sparrow.vballs.config.GlobalConst;
import com.sparrow.vballs.enums.UserDataType;

/**
 * Created by sparrow on 01.05.17.
 */

public class EnemyUserData extends UserData {

    private Vector2 linearVelocity;
    private String[] textureRegions;

    public EnemyUserData(float width, float height, String[] textureRegions) {
        super(width, height);
        userDataType = UserDataType.ENEMY;
        linearVelocity = GlobalConst.ENEMY_LINEAR_VELOCITY;
        this.textureRegions = textureRegions;
    }

    public void setLinearVelocity(Vector2 linearVelocity) {
        this.linearVelocity = linearVelocity;
    }

    public Vector2 getLinearVelocity() {
        return linearVelocity;
    }

    public String[] getTextureRegions() {
        return textureRegions;
    }
}