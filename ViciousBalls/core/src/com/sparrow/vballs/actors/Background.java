package com.sparrow.vballs.actors;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.g2d.Batch;
import com.badlogic.gdx.graphics.g2d.TextureRegion;
import com.badlogic.gdx.math.Rectangle;
import com.badlogic.gdx.scenes.scene2d.Actor;
import com.sparrow.vballs.config.GlobalConst;

/**
 * Created by sparrow on 25.06.17.
 */

public class Background extends Actor {
    private final TextureRegion textureRegion;
    private final TextureRegion background;
    private Rectangle textureRegionBounds1;
    private Rectangle textureRegionBounds2;
    private int speed = 170;

    public Background() {
        background = new TextureRegion(new Texture(Gdx.files.internal(GlobalConst.BACKGROUND_STATIC_PATH)));
        textureRegion = new TextureRegion(new Texture(Gdx.files.internal(GlobalConst.BACKGROUND_DYNAMIC_PATH)));
        textureRegionBounds1 = new Rectangle(0 - GlobalConst.SCREEN_WIDTH / 2, 0, GlobalConst.SCREEN_WIDTH, GlobalConst.SCREEN_HEIGHT);
        textureRegionBounds2 = new Rectangle(GlobalConst.SCREEN_WIDTH / 2, 0, GlobalConst.SCREEN_WIDTH, GlobalConst.SCREEN_WIDTH);
    }

    @Override
    public void act(float delta) {
        if (leftBoundsReached(delta)) {
            resetBounds();
        } else {
            updateXBounds(-delta);
        }
    }

    @Override
    public void draw(Batch batch, float parentAlpha) {
        super.draw(batch, parentAlpha);
        batch.draw(background, 0, 0, GlobalConst.SCREEN_WIDTH, GlobalConst.SCREEN_HEIGHT);
        batch.draw(textureRegion, textureRegionBounds1.x, textureRegionBounds1.y, GlobalConst.SCREEN_WIDTH, GlobalConst.SCREEN_HEIGHT);
        batch.draw(textureRegion, textureRegionBounds2.x, textureRegionBounds2.y, GlobalConst.SCREEN_WIDTH, GlobalConst.SCREEN_HEIGHT);
    }

    private boolean leftBoundsReached(float delta) {
        return (textureRegionBounds2.x - (delta * speed)) <= 0;
    }

    private void updateXBounds(float delta) {
        textureRegionBounds1.x += delta * speed;
        textureRegionBounds2.x += delta * speed;
    }

    private void resetBounds() {
        textureRegionBounds1 = textureRegionBounds2;
        textureRegionBounds2 = new Rectangle(GlobalConst.SCREEN_WIDTH, 0, GlobalConst.SCREEN_WIDTH, GlobalConst.SCREEN_HEIGHT);
    }
}
