package com.sparrow.vballs.actors;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.graphics.g2d.Animation;
import com.badlogic.gdx.graphics.g2d.Batch;
import com.badlogic.gdx.graphics.g2d.TextureAtlas;
import com.badlogic.gdx.graphics.g2d.TextureRegion;
import com.badlogic.gdx.physics.box2d.Body;
import com.badlogic.gdx.physics.box2d.PolygonShape;
import com.sparrow.vballs.config.GlobalConst;
import com.sparrow.vballs.enums.RunnerState;
import com.sparrow.vballs.physics.RunnerUserData;

/**
 * Created by sparrow on 17.04.17.
 */

public class Runner extends GameActor {
    private RunnerState runnerState;
    private Animation runningAnimation;
    private TextureRegion jumpingTexture;
    private TextureRegion dodgingTexture;
    private TextureRegion hitTexture;
    private float stateTime;

    public Runner(Body body) {
        super(body);

        //prepare running animation
        TextureAtlas runningMan = new TextureAtlas(GlobalConst.PLAYER_RUN_ATLAS_PATH);
        TextureRegion[] runningFrames = new TextureRegion[GlobalConst.PLAYER_RUNNING_REGION_NAMES.length];
        for (int i = 0; i < GlobalConst.PLAYER_RUNNING_REGION_NAMES.length; i++) {
            String path = GlobalConst.PLAYER_RUNNING_REGION_NAMES[i];
            runningFrames[i] = runningMan.findRegion(path);
        }
        runningAnimation = new Animation(0.1f, runningFrames);

        //prepare jumping texture
        TextureAtlas jumpingMan = new TextureAtlas(GlobalConst.PLAYER_JUMP_ATLAS_PATH);
        jumpingTexture = jumpingMan.findRegion(GlobalConst.PLAYER_JUMPING_REGION_NAME);

        //prepare dodging texture
        TextureAtlas dodgingMan = new TextureAtlas(GlobalConst.PLAYER_SLIDE_ATLAS_PATH);
        dodgingTexture = dodgingMan.findRegion(GlobalConst.PLAYER_ROLLING_REGION_NAME);

        //prepare hit texture
        TextureAtlas deadMan = new TextureAtlas(GlobalConst.PLAYER_DEAD_ATLAS_PATH);
        hitTexture = deadMan.findRegion(GlobalConst.PLAYER_HIT_REGION_NAME);

        runnerState = RunnerState.RUNNING;
        stateTime = 0f;
    }

    @Override
    public RunnerUserData getUserData() {
        return (RunnerUserData) userData;
    }

    @Override
    public void draw(Batch batch, float parentAlpha) {
        super.draw(batch, parentAlpha);

        float x = screenRectangle.x - (screenRectangle.width * 0.1f);
        float y = screenRectangle.y;
        float width = screenRectangle.width * 1.8f;


        if (runnerState == RunnerState.DODGING) {
            batch.draw(dodgingTexture, x,y + screenRectangle.height / 4, width,
                    screenRectangle.height * 3 / 4);
        } else if (runnerState == RunnerState.HIT) {
            // When he's hit we also want to apply rotation if the body has been rotated
            //userData.setHeight(2f);
            //userData.setWidth(2.46f);
            batch.draw(hitTexture, x, y, width * 0.5f,
                    screenRectangle.height * 0.5f, screenRectangle.width*2, screenRectangle.height, 1f, 1f,
                    (float) Math.toDegrees(body.getAngle()));
        } else if (runnerState == runnerState.JUMPING) {
            batch.draw(jumpingTexture, x, y, width,
                    screenRectangle.height);
        } else {
            // Running
            stateTime += Gdx.graphics.getDeltaTime()*1.8;
            batch.draw((TextureRegion) runningAnimation.getKeyFrame(stateTime, true), x, y, width, screenRectangle.getHeight());
        }
    }

    public void jump(){
        if(runnerState == RunnerState.RUNNING){
            body.applyLinearImpulse(getUserData().getJumpingLinearImpulse(), body.getWorldCenter(), true);
            runnerState = RunnerState.JUMPING;
        }
    }

    public void dodge(){
        if(runnerState == RunnerState.RUNNING){
            ((PolygonShape)body.getFixtureList().get(0).getShape()).setAsBox(GlobalConst.RUNNER_WIDTH/2, GlobalConst.RUNNER_HEIGHT/4);
            body.setTransform(getUserData().getDodgingPosition(), 0);
            runnerState = RunnerState.DODGING;
        }
    }

    public void stopDodge(){
        ((PolygonShape)body.getFixtureList().get(0).getShape()).setAsBox(GlobalConst.RUNNER_WIDTH/2, GlobalConst.RUNNER_HEIGHT/2);
        body.setTransform(getUserData().getRunningPosition(), 0);
        runnerState = RunnerState.RUNNING;
    }

    public void landed(){
        if(runnerState == RunnerState.JUMPING){
            runnerState = RunnerState.RUNNING;
        }
    }

    public boolean isDoging(){
        return runnerState == RunnerState.DODGING;
    }

    public void hit() {
        body.applyAngularImpulse(getUserData().getHitAngularImpulse(), true);
        runnerState = RunnerState.HIT;
    }

    public boolean isHit() {
        return runnerState == RunnerState.HIT;
    }

}
