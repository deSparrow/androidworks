package com.scannerapp.sparrow;

import android.content.Context;
import android.content.SharedPreferences;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;

public class TutorialActivity extends AppCompatActivity {
    private boolean checked = false;
    private SharedPreferences.Editor editor;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_tutorial);

        SharedPreferences sharedPref = getSharedPreferences(MenuActivity.CHECKED_KEY, Context.MODE_PRIVATE);
        editor = sharedPref.edit();
        editor.apply();
    }

    public void introducingCheck(View view) {
        checked = !checked;
    }

    public void letsGo(View view) {
        editor.putBoolean("tutorialActivity.checked", checked);
        editor.commit();
        finish();
    }
}
