package com.scannerapp.sparrow;

import android.app.ProgressDialog;
import android.content.Intent;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import com.google.zxing.integration.android.IntentIntegrator;
import com.google.zxing.integration.android.IntentResult;
import com.loopj.android.http.AsyncHttpClient;
import com.loopj.android.http.JsonHttpResponseHandler;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.UnsupportedEncodingException;
import java.net.URLEncoder;

public class ApiActivity extends AppCompatActivity {
    public static final String QUERY = "http://svcs.ebay.com/services/search/FindingService/v1?SECURITY-APPNAME=FabianOl-ScannerA-PRD-e45ed6035-90047e35&OPERATION-NAME=findItemsByProduct&SERVICE-VERSION=1.0.0&RESPONSE-DATA-FORMAT=JSON&REST-PAYLOAD&productId.@type=UPC&paginationInput.entriesPerPage=10&productId=";
    public static final String IMAGE_URL_KEY = "com.scannerapp.sparrow.image_url_key";
    public static final String TITLE_KEY = "com.scannerapp.sparrow.title_key";
    public static final String LOCATION_KEY = "com.scannerapp.sparrow.location_key";
    public static final String COUNTRY_KEY = "com.scannerapp.sparrow.country_key";
    public static final String CATEGORY_KEY = "com.scannerapp.sparrow.category_key";
    public static final String LOCATIONS_TO_SELL_KEY = "com.scannerapp.sparrow.locations_to_sell_key";
    public static final String CURRENCY_KEY = "com.scannerapp.sparrow.currency_key";
    public static final String COST_KEY = "com.scannerapp.sparrow.cost_key";
    private String barCode;
    private Button buttonScanner;
    private ProgressDialog progressDialog;
    private JsonAdapter jsonAdapter;
    private ListView mainListView;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_api);

        jsonAdapter = new JsonAdapter(this, getLayoutInflater());

        mainListView = (ListView) findViewById( R.id.list_from_api);
        mainListView.setAdapter(jsonAdapter);
        mainListView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> adapterView, View view, int position, long l) {
                JSONObject jsonObject = (JSONObject) jsonAdapter.getItem(position);

                String imageURL = jsonObject.optString("galleryURL", "");
                String title = jsonObject.optString("title", "");
                String location = jsonObject.optString("location", "");
                String country = jsonObject.optString("country", "");

                JSONArray jA;
                jA = jsonObject.optJSONArray("primaryCategory");
                JSONObject object = jA.optJSONObject(0);
                String category = object.optString("categoryName", "");

                jA = jsonObject.optJSONArray("shippingInfo");
                object = jA.optJSONObject(0);
                String locationsToSell = object.optString("shipToLocations", "");

                jA = jsonObject.optJSONArray("sellingStatus");
                jsonObject = jA.optJSONObject(0);
                jA = jsonObject.optJSONArray("currentPrice");
                jsonObject = jA.optJSONObject(0);
                String currency = jsonObject.optString("@currencyId", "");
                String cost = jsonObject.optString("__value__", "");

                if(!imageURL.isEmpty()) {
                    imageURL = imageURL.substring(2, imageURL.length() - 2);
                    for (int i = 0; i < imageURL.length(); i++)
                        if (imageURL.charAt(i) == '\\')
                            imageURL = imageURL.substring(0, i) + imageURL.substring(i + 1, imageURL.length());
                }
                if(!title.isEmpty()){
                    title = title.substring(2, title.length()-2);
                }
                if(!category.isEmpty()){
                    category = category.substring(2, category.length()-2);
                }
                if(!location.isEmpty()){
                    location = location.substring(2, location.length()-2);
                }
                if(!country.isEmpty()){
                    country = country.substring(2, country.length()-2);
                }
                if(!locationsToSell.isEmpty()){
                    locationsToSell = locationsToSell.substring(1, locationsToSell.length()-1);
                }

                Intent invokeDetailActivity = new Intent(getApplicationContext(), DetailActivity.class);

                invokeDetailActivity.putExtra(IMAGE_URL_KEY, imageURL);
                invokeDetailActivity.putExtra(TITLE_KEY, title);
                invokeDetailActivity.putExtra(CATEGORY_KEY, category);
                invokeDetailActivity.putExtra(LOCATION_KEY, location);
                invokeDetailActivity.putExtra(COUNTRY_KEY, country);
                invokeDetailActivity.putExtra(LOCATIONS_TO_SELL_KEY, locationsToSell);
                invokeDetailActivity.putExtra(CURRENCY_KEY, currency);
                invokeDetailActivity.putExtra(COST_KEY, cost);

                startActivity(invokeDetailActivity);
            }
        });

        buttonScanner = (Button)findViewById(R.id.buttonScanner);
        buttonScanner.setOnClickListener(e -> {
            IntentIntegrator intentIntegrator = new IntentIntegrator(this);
            intentIntegrator.setDesiredBarcodeFormats(IntentIntegrator.PRODUCT_CODE_TYPES);
            intentIntegrator.setPrompt(getString(R.string.activity_api_prompt_message));
            intentIntegrator.setCameraId(0);
            intentIntegrator.setBeepEnabled(false);
            intentIntegrator.setBarcodeImageEnabled(false);
            intentIntegrator.initiateScan();
        });

        progressDialog = new ProgressDialog(this);
        progressDialog.setMessage(getString(R.string.activity_api_progress_dialog_message));
        progressDialog.setCancelable(false);
    }

    private void queryBooks() {
        String urlString = "";
        try {
            urlString = URLEncoder.encode(barCode, "UTF-8");
        } catch (UnsupportedEncodingException e) {

            e.printStackTrace();
            Toast.makeText(this, R.string.activity_api_error_alert + e.getMessage(), Toast.LENGTH_LONG).show();
        }

        AsyncHttpClient client = new AsyncHttpClient();
        progressDialog.show();

        client.get(QUERY + urlString,
                new JsonHttpResponseHandler() {

                    @Override
                    public void onSuccess(JSONObject jsonObject) {
                        progressDialog.dismiss();

                        JSONArray jA;
                        jA = jsonObject.optJSONArray("findItemsByProductResponse");
                        jsonObject = jA.optJSONObject(0);
                        jA = jsonObject.optJSONArray("searchResult");
                        if(jA == null){
                            Toast.makeText(getApplicationContext(), R.string.activity_api_no_match_alert, Toast.LENGTH_SHORT).show();
                            return;
                        }
                        jsonObject = jA.optJSONObject(0);
                        jA = jsonObject.optJSONArray("item");

                        jsonAdapter.updateData(jA);

                    }

                    @Override
                    public void onFailure(int statusCode, Throwable throwable, JSONObject error) {
                        progressDialog.dismiss();
                        Toast.makeText(getApplicationContext(), getString(R.string.activity_api_error_alert) + statusCode + " " + throwable.getMessage(), Toast.LENGTH_LONG).show();
                    }
                });
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        IntentResult result = IntentIntegrator.parseActivityResult(requestCode, resultCode, data);
        if(result != null){
            if(result.getContents() == null) {
                barCode = "";
                Toast.makeText(this, R.string.activity_api_scanning_failed_alert, Toast.LENGTH_SHORT).show();
            }
            else{
                barCode = result.getContents();

                queryBooks();
            }
        }
        else
            super.onActivityResult(requestCode, resultCode, data);
    }
}
