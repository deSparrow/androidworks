package com.scannerapp.sparrow;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.scannerapp.sparrow.model.Product;
import com.squareup.picasso.Picasso;

public class DetailActivityBasket extends AppCompatActivity {

    private String productImgUrl;
    private TextView productName;
    private TextView productCategory;
    private TextView productLocation;
    private TextView productCountry;
    private TextView productShippingLocations;
    private TextView productCurrency;
    private TextView productCost;
    private int index;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_detail_basket);

        String extra;
        if(!(extra = this.getIntent().getExtras().getString(ApiActivity.TITLE_KEY)).isEmpty()){
            productName = (TextView)findViewById(R.id.activity_detail_product_name_content);
            productName.setText(extra);
        }
        if(!(extra = this.getIntent().getExtras().getString(ApiActivity.CATEGORY_KEY)).isEmpty()){
            productCategory = (TextView)findViewById(R.id.activity_detail_product_category_content);
            productCategory.setText(extra);
        }
        if(!(extra = this.getIntent().getExtras().getString(ApiActivity.LOCATION_KEY)).isEmpty()){
            productLocation = (TextView)findViewById(R.id.activity_detail_product_location_content);
            productLocation.setText(extra);
        }
        if(!(extra = this.getIntent().getExtras().getString(ApiActivity.COUNTRY_KEY)).isEmpty()){
            productCountry = (TextView)findViewById(R.id.activity_detail_product_country_content);
            productCountry.setText(extra);
        }
        if(!(extra = this.getIntent().getExtras().getString(ApiActivity.LOCATIONS_TO_SELL_KEY)).isEmpty()){
            productShippingLocations = (TextView)findViewById(R.id.activity_detail_product_shipping_locations_content);
            productShippingLocations.setText(extra);
        }
        if(!(extra = this.getIntent().getExtras().getString(ApiActivity.CURRENCY_KEY)).isEmpty()){
            productCurrency = (TextView)findViewById(R.id.activity_detail_product_currency_content);
            productCurrency.setText(extra);
        }
        if(!(extra = this.getIntent().getExtras().getString(ApiActivity.COST_KEY)).isEmpty()){
            productCost = (TextView)findViewById(R.id.activity_detail_product_cost_content);
            productCost.setText(extra);
        }

        if(!(productImgUrl = this.getIntent().getExtras().getString(ApiActivity.IMAGE_URL_KEY)).isEmpty()){
            ImageView productImg = (ImageView)findViewById(R.id.activity_detail_product_img);
            Picasso.with(this).load(productImgUrl).placeholder(R.drawable.ic_not_interested_purple_700_48dp).into(productImg);
        }

        index = this.getIntent().getExtras().getInt(BasketActivity.INDEX_PRODUCT_KEY);
    }

    public void deleteProduct(View view) {
        BasketActivity.products.remove(index);
        if(BasketActivity.products.size()>0) {
            Toast.makeText(getApplicationContext(), R.string.activity_detail_basket_product_removed_alert, Toast.LENGTH_SHORT).show();
        }
        finish();
    }

    public void returnToParent(View view) {
        finish();
    }
}
