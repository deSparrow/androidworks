package com.scannerapp.sparrow;

import android.content.Intent;
import android.database.sqlite.SQLiteException;
import android.support.annotation.NonNull;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.AdapterView;
import android.widget.EditText;
import android.widget.ListView;
import android.widget.Toast;

import com.afollestad.materialdialogs.DialogAction;
import com.afollestad.materialdialogs.MaterialDialog;
import com.scannerapp.sparrow.model.Product;

import java.util.ArrayList;
import java.util.List;

public class BasketActivity extends AppCompatActivity {
    public static final String INDEX_PRODUCT_KEY = "com.scannerapp.sparrow.index_product_key";
    public ProductAdapter productAdapter;
    private ListView lvProducts;
    public static List<Product> products = new ArrayList<>();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_basket);

        lvProducts = (ListView) findViewById( R.id.activity_basket_listview);
        productAdapter = new ProductAdapter(this, products);
        lvProducts.setAdapter(productAdapter);

        lvProducts.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int i, long l) {
                new MaterialDialog.Builder(view.getContext())
                        .title(R.string.activity_basket_title_alertbox)
                        .content(R.string.activity_basket_content_alertbox)
                        .onNegative(new MaterialDialog.SingleButtonCallback() {
                            @Override
                            public void onClick(@NonNull MaterialDialog dialog, @NonNull DialogAction which) {
                                try {
                                    Product item = (Product) parent.getItemAtPosition(i);
                                    productAdapter.remove(item);
                                    productAdapter.notifyDataSetChanged();
                                    if(products.isEmpty()){
                                        finish();
                                    }
                                } catch (SQLiteException e){
                                    Toast.makeText(getApplicationContext(), R.string.activity_basket_delete_went_wrong_alert, Toast.LENGTH_SHORT).show();
                                    return;
                                }
                                Toast.makeText(getApplicationContext(), R.string.activity_basket_product_removed_alert, Toast.LENGTH_SHORT).show();
                                dialog.dismiss();
                            }
                        })
                        .onPositive(new MaterialDialog.SingleButtonCallback() {
                            @Override
                            public void onClick(@NonNull MaterialDialog dialog, @NonNull DialogAction which) {
                                Intent invokeDetailActivity = new Intent(getApplicationContext(), DetailActivityBasket.class);

                                Product item = (Product) parent.getItemAtPosition(i);
                                invokeDetailActivity.putExtra(ApiActivity.IMAGE_URL_KEY, item.getProductImgUrl());
                                invokeDetailActivity.putExtra(ApiActivity.TITLE_KEY, item.getProductName());
                                invokeDetailActivity.putExtra(ApiActivity.CATEGORY_KEY, item.getProductCategory());
                                invokeDetailActivity.putExtra(ApiActivity.LOCATION_KEY, item.getProductLocation());
                                invokeDetailActivity.putExtra(ApiActivity.COUNTRY_KEY, item.getProductCountry());
                                invokeDetailActivity.putExtra(ApiActivity.LOCATIONS_TO_SELL_KEY, item.getProductShippingLocations());
                                invokeDetailActivity.putExtra(ApiActivity.CURRENCY_KEY, item.getProductCurrency());
                                invokeDetailActivity.putExtra(ApiActivity.COST_KEY, item.getProductCost());
                                invokeDetailActivity.putExtra(INDEX_PRODUCT_KEY, i);

                                startActivity(invokeDetailActivity);
                                dialog.dismiss();
                            }
                        })
                        .onNeutral(new MaterialDialog.SingleButtonCallback() {
                            @Override
                            public void onClick(@NonNull MaterialDialog dialog, @NonNull DialogAction which) {
                                dialog.dismiss();
                            }
                        })
                        .positiveText(R.string.activity_basket_positive_alertbox)
                        .negativeText(R.string.activity_basket_negative_alertbox)
                        .neutralText(R.string.activity_basket_neutral_alertbox)
                        .cancelable(true)
                        .show();
            }


        });
    }

    @Override
    protected void onStart() {
        super.onStart();
        productAdapter.notifyDataSetChanged();
        if(productAdapter.mylist.isEmpty()){
            Toast.makeText(this, R.string.activity_basket_no_product_alert, Toast.LENGTH_SHORT).show();
            finish();
        }
    }
}
