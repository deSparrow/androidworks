package com.scannerapp.sparrow;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import com.squareup.picasso.Picasso;

import org.json.JSONArray;
import org.json.JSONObject;

import java.io.UnsupportedEncodingException;
import java.net.URLEncoder;

/**
 * Created by sparrow on 28.12.16.
 */

public class JsonAdapter extends BaseAdapter {

    Context mContext;
    LayoutInflater mInflater;
    JSONArray mJsonArray;

    public JsonAdapter(Context context, LayoutInflater inflater) {
        mContext = context;
        mInflater = inflater;
        mJsonArray = new JSONArray();
    }

    // this is used so you only ever have to do
    // inflation and finding by ID once ever per View
    private static class ViewHolder {
        public ImageView iconImageView;
        public TextView titleTextView;
        public TextView priceTextView;
    }

    public void updateData(JSONArray jsonArray) {
        // update the adapter's dataset
        mJsonArray = jsonArray;
        notifyDataSetChanged();
    }

    @Override
    public int getCount() {
        return mJsonArray.length();
    }

    @Override
    public Object getItem(int position) {
        return mJsonArray.optJSONObject(position);
    }

    @Override
    public long getItemId(int position) {
        // your particular dataset uses String IDs
        // but you have to put something in this method
        return position;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        ViewHolder holder;

        // check if the view already exists
        // if so, no need to inflate and findViewById again!
        if (convertView == null) {

            // Inflate the custom row layout from your XML.
            convertView = mInflater.inflate(R.layout.product_template_row, null);

            // create a new "Holder" with subviews
            holder = new ViewHolder();
            holder.iconImageView = (ImageView) convertView.findViewById(R.id.img_product_template);
            holder.titleTextView = (TextView) convertView.findViewById(R.id.title_product_template);
            holder.priceTextView = (TextView) convertView.findViewById(R.id.price_product_template);

            // hang onto this holder for future recyclage
            convertView.setTag(holder);
        } else {

            // skip all the expensive inflation/findViewById
            // and just get the holder you already made
            holder = (ViewHolder) convertView.getTag();
        }

        // Get the current book's data in JSON form
        JSONObject jsonObject = (JSONObject) getItem(position);

// See if there is a cover ID in the Object
        if (jsonObject.has("galleryURL")) {

            // Construct the image URL (specific to API)
            String imageURL = jsonObject.optString("galleryURL");
            imageURL = imageURL.substring(2, imageURL.length()-2);
            for(int i = 0; i < imageURL.length(); i++)
                if(imageURL.charAt(i) == '\\')
                    imageURL = imageURL.substring(0, i) + imageURL.substring(i + 1, imageURL.length());

            Picasso.with(mContext).load(imageURL).placeholder(R.drawable.ic_not_interested_purple_700_48dp).into(holder.iconImageView);
        } else {

            // If there is no cover ID in the object, use a placeholder
            holder.iconImageView.setImageResource(R.drawable.ic_not_interested_purple_700_48dp);
        }

        // Grab the title and author from the JSON
        String productTitle = "";
        String price = "";

        if (jsonObject.has("title")) {
            String title = jsonObject.optString("title");
            productTitle = title.substring(2, title.length()-2);
        }

        if (jsonObject.has("sellingStatus")) {
            JSONArray jA;
            jA = jsonObject.optJSONArray("sellingStatus");
            jsonObject = jA.optJSONObject(0);
            jA = jsonObject.optJSONArray("currentPrice");
            jsonObject = jA.optJSONObject(0);
            String currency, value;
            currency = jsonObject.optString("@currencyId");
            value = jsonObject.optString("__value__");
            price = value+" "+currency;
        }

// Send these Strings to the TextViews for display
        holder.titleTextView.setText(productTitle);
        holder.priceTextView.setText(price);

        return convertView;
    }
}
