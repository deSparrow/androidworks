package com.scannerapp.sparrow;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.scannerapp.sparrow.model.Product;
import com.squareup.picasso.Picasso;

import java.util.List;

/**
 * Created by sparrow on 29.12.16.
 */

public class ProductAdapter extends ArrayAdapter<Product> {
    List<Product> mylist;
    Context context;

    public ProductAdapter(Context _context, List<Product> _mylist) {
        super(_context, R.layout.product_template_row, _mylist);
        this.context = _context;
        this.mylist = _mylist;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        convertView = new LinearLayout(getContext());
        String inflater = Context.LAYOUT_INFLATER_SERVICE;
        LayoutInflater vi = (LayoutInflater)getContext().getSystemService(inflater);
        convertView = vi.inflate(R.layout.product_template_row, parent, false);

        Product product = getItem(position);

        TextView txtTitle = (TextView) convertView.findViewById(R.id.title_product_template);
        txtTitle.setText(product.getProductName());

        TextView txtPrice = (TextView) convertView.findViewById(R.id.price_product_template);
        txtPrice.setText(product.getProductCost()+" "+product.getProductCurrency());

        ImageView img = (ImageView)convertView.findViewById(R.id.img_product_template);
        Picasso.with(context).load(product.getProductImgUrl()).placeholder(R.drawable.ic_not_interested_purple_700_48dp).into(img);

        return convertView;
    }

}