package com.scannerapp.sparrow.model;

/**
 * Created by sparrow on 29.12.16.
 */

public class Product {
    private String productImgUrl;
    private String productName;
    private String productCategory;
    private String productLocation;
    private String productCountry;
    private String productShippingLocations;
    private String productCurrency;
    private String productCost;

    public String getProductImgUrl() {
        return productImgUrl;
    }

    public void setProductImgUrl(String productImgUrl) {
        this.productImgUrl = productImgUrl;
    }

    public String getProductName() {
        return productName;
    }

    public void setProductName(String productName) {
        this.productName = productName;
    }

    public String getProductCategory() {
        return productCategory;
    }

    public void setProductCategory(String productCategory) {
        this.productCategory = productCategory;
    }

    public String getProductLocation() {
        return productLocation;
    }

    public void setProductLocation(String productLocation) {
        this.productLocation = productLocation;
    }

    public String getProductCountry() {
        return productCountry;
    }

    public void setProductCountry(String productCountry) {
        this.productCountry = productCountry;
    }

    public String getProductShippingLocations() {
        return productShippingLocations;
    }

    public void setProductShippingLocations(String productShippingLocations) {
        this.productShippingLocations = productShippingLocations;
    }

    public String getProductCurrency() {
        return productCurrency;
    }

    public void setProductCurrency(String productCurrency) {
        this.productCurrency = productCurrency;
    }

    public String getProductCost() {
        return productCost;
    }

    public void setProductCost(String productCost) {
        this.productCost = productCost;
    }
}
