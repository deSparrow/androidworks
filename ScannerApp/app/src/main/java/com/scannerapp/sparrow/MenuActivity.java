package com.scannerapp.sparrow;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Toast;

public class MenuActivity extends AppCompatActivity {
    public static final String CHECKED_KEY = "com.scannerapp.sparrow";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_menu);

        SharedPreferences sharedPref = getSharedPreferences(CHECKED_KEY, Context.MODE_PRIVATE);
        boolean checked = sharedPref.getBoolean("tutorialActivity.checked", false);
        if(!checked){
            invokeTutorialActivity();
        }
    }

    private void invokeTutorialActivity(){
        Intent intent = new Intent(this, TutorialActivity.class);
        startActivity(intent);
    }

    public void invokeApiActivity(View view) {
        startActivity(new Intent(this, ApiActivity.class));
    }

    public void invokeBasketActivity(View view) {
        if(BasketActivity.products.isEmpty()){
            Toast.makeText(this, R.string.activity_menu_no_products_alert, Toast.LENGTH_SHORT).show();
            return;
        }
        startActivity(new Intent(this, BasketActivity.class));
    }

    public void exitApplication(View view) {
        finish();
    }
}
