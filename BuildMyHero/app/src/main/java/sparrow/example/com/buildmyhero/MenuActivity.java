package sparrow.example.com.buildmyhero;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;

public class MenuActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_menu);

        SharedPreferences sharedPref = getSharedPreferences("sparrow.example.com.buildmyhero", Context.MODE_PRIVATE);
        boolean checked = sharedPref.getBoolean("tutorialActivity.checked", false);
        if(!checked){
            invokeTutorialActivity();
        }
    }

    private void invokeTutorialActivity(){
        Intent intent = new Intent(this, TutorialActivity.class);
        startActivity(intent);
    }

    public void viewCharacter(View view) {
    }

    public void createCharacter(View view) {
    }

    public void deleteCharacter(View view) {
    }

    public void exitApplication(View view) {
    }
}
