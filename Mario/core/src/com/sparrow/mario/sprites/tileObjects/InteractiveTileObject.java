package com.sparrow.mario.sprites.tileObjects;

import com.badlogic.gdx.maps.MapObject;
import com.badlogic.gdx.maps.objects.RectangleMapObject;
import com.badlogic.gdx.maps.tiled.TiledMap;
import com.badlogic.gdx.maps.tiled.TiledMapTile;
import com.badlogic.gdx.maps.tiled.TiledMapTileLayer;
import com.badlogic.gdx.math.Rectangle;
import com.badlogic.gdx.physics.box2d.Body;
import com.badlogic.gdx.physics.box2d.BodyDef;
import com.badlogic.gdx.physics.box2d.Filter;
import com.badlogic.gdx.physics.box2d.Fixture;
import com.badlogic.gdx.physics.box2d.FixtureDef;
import com.badlogic.gdx.physics.box2d.PolygonShape;
import com.badlogic.gdx.physics.box2d.World;
import com.sparrow.mario.Mario;
import com.sparrow.mario.screens.PlayScreen;
import com.sparrow.mario.sprites.MarioSprite;

/**
 * Created by sparrow on 09.09.16.
 */
public abstract class InteractiveTileObject {
    protected World world;
    protected TiledMap tiledMap;
    protected TiledMapTile tiledMapTile;
    protected Rectangle bounds;
    protected Body body;
    protected Fixture fixture;
    protected PlayScreen playScreen;
    protected MapObject object;

    public InteractiveTileObject(PlayScreen playScreen, MapObject object){
        this.object = object;
        this.playScreen = playScreen;
        this.world = playScreen.getWorld();
        this.tiledMap = playScreen.getTiledMap();
        this.bounds = ((RectangleMapObject)object).getRectangle();

        BodyDef bodyDef = new BodyDef();
        FixtureDef fixtureDef = new FixtureDef();
        PolygonShape shape = new PolygonShape();

        bodyDef.type = BodyDef.BodyType.StaticBody;
        bodyDef.position.set((bounds.getX() + bounds.getWidth()/2)/ Mario.PPM, (bounds.getY() + bounds.getHeight()/2)/Mario.PPM);
        body = world.createBody(bodyDef);
        shape.setAsBox(bounds.getWidth()/2/Mario.PPM, bounds.getHeight()/2/Mario.PPM);
        fixtureDef.shape = shape;
        fixture = body.createFixture(fixtureDef);
    }

    public abstract void onHeadHit(MarioSprite marioSprite);

    public void setCategoryFilter(short bitFilter){
        Filter filter = new Filter();
        filter.categoryBits = bitFilter;
        fixture.setFilterData(filter);
    }

    public TiledMapTileLayer.Cell getCell(){
        TiledMapTileLayer tiledMapTileLayer = (TiledMapTileLayer)tiledMap.getLayers().get(1);
        return tiledMapTileLayer.getCell((int)(body.getPosition().x * Mario.PPM / 16), (int)(body.getPosition().y * Mario.PPM / 16));
    }
}
