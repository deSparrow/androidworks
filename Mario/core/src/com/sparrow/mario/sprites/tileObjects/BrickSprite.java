package com.sparrow.mario.sprites.tileObjects;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.audio.Sound;
import com.badlogic.gdx.maps.MapObject;
import com.badlogic.gdx.math.Rectangle;
import com.sparrow.mario.Mario;
import com.sparrow.mario.scenes.Hud;
import com.sparrow.mario.screens.PlayScreen;
import com.sparrow.mario.sprites.MarioSprite;
import com.sparrow.mario.sprites.tileObjects.InteractiveTileObject;

/**
 * Created by sparrow on 09.09.16.
 */
public class BrickSprite extends InteractiveTileObject {
    private static final int BRICK_SCORE = 200;

    public BrickSprite(PlayScreen playScreen, MapObject object) {
        super(playScreen, object);
        fixture.setUserData(this);
        setCategoryFilter(Mario.BRICK_BIT);
    }

    @Override
    public void onHeadHit(MarioSprite marioSprite) {
        if(marioSprite.isMarioIsBig()) {
            setCategoryFilter(Mario.DESTROYED_BIT);
            getCell().setTile(null);
            Hud.addScore(BRICK_SCORE);
            Mario.assetManager.get("music/sounds/breakblock.wav", Sound.class).play();
        }
        else {
            Mario.assetManager.get("music/sounds/bump.wav", Sound.class).play();
        }
    }
}
