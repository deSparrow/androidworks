package com.sparrow.mario.sprites.tileObjects;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.audio.Sound;
import com.badlogic.gdx.maps.MapObject;
import com.badlogic.gdx.maps.tiled.TiledMapTileSet;
import com.badlogic.gdx.math.Rectangle;
import com.badlogic.gdx.math.Vector2;
import com.sparrow.mario.Mario;
import com.sparrow.mario.scenes.Hud;
import com.sparrow.mario.screens.PlayScreen;
import com.sparrow.mario.sprites.MarioSprite;
import com.sparrow.mario.sprites.items.ItemDefinition;
import com.sparrow.mario.sprites.items.Mushroom;
import com.sparrow.mario.sprites.tileObjects.InteractiveTileObject;

/**
 * Created by sparrow on 09.09.16.
 */
public class CoinSprite extends InteractiveTileObject {
    private final int BLANK_COIN_ID = 28;
    private static TiledMapTileSet tileSet;
    private static final int COIN_SCORE = 200;

    public CoinSprite(PlayScreen playScreen, MapObject object) {
        super(playScreen, object);
        tileSet = tiledMap.getTileSets().getTileSet("tileset");
        fixture.setUserData(this);
        setCategoryFilter(Mario.COIN_BIT);
    }

    @Override
    public void onHeadHit(MarioSprite mario) {
        Gdx.app.log("Coin", "Coin hit");
        if(getCell().getTile().getId() == BLANK_COIN_ID){
            Mario.assetManager.get("music/sounds/bump.wav", Sound.class).play();
        }else{
            if(object.getProperties().containsKey("mushroom")){
                playScreen.spawnItem(new ItemDefinition(new Vector2(body.getPosition().x, body.getPosition().y + 16 / Mario.PPM), Mushroom.class));
                Mario.assetManager.get("music/sounds/powerup_spawn.wav", Sound.class).play();
            }
            else {
                Mario.assetManager.get("music/sounds/coin.wav", Sound.class).play();
            }
        }
        getCell().setTile(tileSet.getTile(BLANK_COIN_ID));
        Hud.addScore(COIN_SCORE);
    }
}
