package com.sparrow.mario.sprites.items;

import com.badlogic.gdx.math.Vector2;
import com.badlogic.gdx.physics.box2d.BodyDef;
import com.badlogic.gdx.physics.box2d.CircleShape;
import com.badlogic.gdx.physics.box2d.FixtureDef;
import com.sparrow.mario.Mario;
import com.sparrow.mario.screens.PlayScreen;
import com.sparrow.mario.sprites.MarioSprite;

/**
 * Created by sparrow on 15.09.16.
 */
public class Mushroom extends Item {
    public Mushroom(PlayScreen playScreen, float x, float y) {
        super(playScreen, x, y);
        setRegion(playScreen.getTextureAtlas().findRegion("mushroom"), 0, 0, 16, 16);
        velocity = new Vector2(0.7f, 0);
    }

    @Override
    public void defineItem() {
        BodyDef bodyDef = new BodyDef();
        bodyDef.position.set(getX(), getY());
        bodyDef.type = BodyDef.BodyType.DynamicBody;
        body = world.createBody(bodyDef);

        FixtureDef fixtureDef = new FixtureDef();
        CircleShape circleShape = new CircleShape();
        circleShape.setRadius(6/ Mario.PPM);
        fixtureDef.filter.categoryBits = Mario.ITEM_BIT;
        fixtureDef.filter.maskBits = Mario.MARIO_BIT | Mario.OBJECT_BIT | Mario.GROUND_BIT | Mario.COIN_BIT | Mario.BRICK_BIT;

        fixtureDef.shape = circleShape;
        body.createFixture(fixtureDef).setUserData(this);
    }

    @Override
    public void use(MarioSprite marioSprite) {
        destroy();
        marioSprite.grow();
    }

    @Override
    public void update(float deltaTime) {
        super.update(deltaTime);
        setPosition(body.getPosition().x-getWidth()/2, body.getPosition().y-getHeight()/2);
        velocity.y = body.getLinearVelocity().y;
        body.setLinearVelocity(velocity);
    }
}
