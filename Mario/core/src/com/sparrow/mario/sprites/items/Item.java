package com.sparrow.mario.sprites.items;

import com.badlogic.gdx.graphics.g2d.Batch;
import com.badlogic.gdx.graphics.g2d.Sprite;
import com.badlogic.gdx.math.Vector2;
import com.badlogic.gdx.physics.box2d.Body;
import com.badlogic.gdx.physics.box2d.World;
import com.sparrow.mario.Mario;
import com.sparrow.mario.screens.PlayScreen;
import com.sparrow.mario.sprites.MarioSprite;

/**
 * Created by sparrow on 15.09.16.
 */
public abstract class Item extends Sprite {
    protected PlayScreen playScreen;
    protected World world;
    protected Vector2 velocity;
    protected boolean toDestroy, isDestroyed;
    protected Body body;

    public Item(PlayScreen playScreen, float x, float y){
        this.playScreen = playScreen;
        this.world = playScreen.getWorld();
        setPosition(x, y);
        setBounds(getX(), getY(), 16/ Mario.PPM, 16/Mario.PPM);
        defineItem();
        toDestroy = false;
        isDestroyed = false;
    }

    public abstract void defineItem();
    public abstract void use(MarioSprite marioSprite);

    public void update(float deltaTime){
        if(toDestroy && !isDestroyed){
            world.destroyBody(body);
            isDestroyed = true;
        }
    }

    public void destroy(){
        toDestroy = true;
    }

    public void draw(Batch batch){
        if(!isDestroyed){
            super.draw(batch);
        }
    }

    public void reverseVelocity(boolean x, boolean y){
        if(x){
            velocity.x = -velocity.x;
        }
        if(y){
            velocity.y = -velocity.y;
        }
    }
}
