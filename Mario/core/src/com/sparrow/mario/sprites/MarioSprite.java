package com.sparrow.mario.sprites;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.audio.Music;
import com.badlogic.gdx.audio.Sound;
import com.badlogic.gdx.graphics.g2d.Animation;
import com.badlogic.gdx.graphics.g2d.Sprite;
import com.badlogic.gdx.graphics.g2d.TextureRegion;
import com.badlogic.gdx.math.Vector2;
import com.badlogic.gdx.physics.box2d.Body;
import com.badlogic.gdx.physics.box2d.BodyDef;
import com.badlogic.gdx.physics.box2d.CircleShape;
import com.badlogic.gdx.physics.box2d.EdgeShape;
import com.badlogic.gdx.physics.box2d.Filter;
import com.badlogic.gdx.physics.box2d.Fixture;
import com.badlogic.gdx.physics.box2d.FixtureDef;
import com.badlogic.gdx.physics.box2d.World;
import com.badlogic.gdx.utils.Array;
import com.sparrow.mario.Mario;
import com.sparrow.mario.screens.PlayScreen;
import com.sparrow.mario.sprites.enemies.Enemy;
import com.sparrow.mario.sprites.enemies.Turtle;

/**
 * Created by sparrow on 08.09.16.
 */
public class MarioSprite extends Sprite {
    public enum State {RUNNING, STANDING, JUMPING, FALLING, GROWING, DEAD}
    public State currentState, previousState;
    private Animation marioRun;
    private boolean runningRight;
    private boolean timeToRedefineMario;
    private boolean marioIsBig;
    private boolean runGrowAnimation;
    private boolean timeToDefineBigMario;
    private boolean marioIsDead;
    private float stateTimer;
    public World world;
    public Body body;
    private TextureRegion marioStand, marioJump, bigMarioStand, bigMarioJump, marioDead;
    private Animation bigMarioRun, growMario;

    public MarioSprite(PlayScreen playScreen){
        this.world = playScreen.getWorld();
        defineMario();
        marioStand = new TextureRegion(playScreen.getTextureAtlas().findRegion("little_mario"), 0, 0, 16, 16);
        bigMarioStand = new TextureRegion(playScreen.getTextureAtlas().findRegion("big_mario"), 0, 0, 16, 32);

        setBounds(0, 0, 16/Mario.PPM, 16/Mario.PPM);
        setRegion(marioStand);

        currentState = State.STANDING;
        previousState = State.STANDING;
        runningRight = true;
        stateTimer = 0;
        Array<TextureRegion> animationFrames = new Array<TextureRegion>();
        for(int i = 1; i<4; ++i){
            animationFrames.add(new TextureRegion(playScreen.getTextureAtlas().findRegion("little_mario"), i*16, 0, 16, 16));
        }
        marioRun = new Animation(0.1f, animationFrames);

        animationFrames.clear();
        for(int i = 1; i<4; ++i){
            animationFrames.add(new TextureRegion(playScreen.getTextureAtlas().findRegion("big_mario"), i*16, 0, 16, 32));
        }
        bigMarioRun = new Animation(0.1f, animationFrames);

        animationFrames.clear();

        animationFrames.add(new TextureRegion(playScreen.getTextureAtlas().findRegion("big_mario"), 240, 0, 16, 32));
        animationFrames.add(new TextureRegion(playScreen.getTextureAtlas().findRegion("big_mario"), 0, 0, 16, 32));
        animationFrames.add(new TextureRegion(playScreen.getTextureAtlas().findRegion("big_mario"), 240, 0, 16, 32));
        animationFrames.add(new TextureRegion(playScreen.getTextureAtlas().findRegion("big_mario"), 0, 0, 16, 32));
        growMario = new Animation(0.2f, animationFrames);

        marioJump = new TextureRegion(playScreen.getTextureAtlas().findRegion("little_mario"), 80, 0, 16, 16);
        bigMarioJump = new TextureRegion(playScreen.getTextureAtlas().findRegion("big_mario"), 80, 0, 16, 32);

        marioDead = new TextureRegion(playScreen.getTextureAtlas().findRegion("little_mario"), 96, 0, 16, 16);

        animationFrames.clear();
    }

    public void hit(Enemy enemy) {
        if(enemy instanceof Turtle && ((Turtle)enemy).getCurrentState()== Turtle.State.STANDING_SHELL){
            ((Turtle)enemy).kick(this.getX() <= enemy.getX() ? Turtle.KICK_RIGHT_SPEED : Turtle.KICK_LEFT_SPEED);
        }else {
            if (isMarioIsBig()) {
                marioIsBig = false;
                timeToRedefineMario = true;
                setBounds(getX(), getY(), getWidth(), getHeight() / 2);
                Mario.assetManager.get("music/sounds/powerdown.wav", Sound.class).play();
            } else {
                Mario.assetManager.get("music/theme/theme_music.ogg", Music.class).stop();
                Mario.assetManager.get("music/sounds/mariodie.wav", Sound.class).play();
                marioIsDead = true;
                Filter filter = new Filter();
                filter.maskBits = Mario.NOTHING_BIT;
                for (Fixture fixture : body.getFixtureList()) {
                    fixture.setFilterData(filter);
                }
                body.applyLinearImpulse(new Vector2(0, 4f), body.getWorldCenter(), true);
            }
        }
    }

    public boolean isMarioIsBig() {
        return marioIsBig;
    }

    public void update(float deltaTime){
        if(marioIsBig){
            setPosition(body.getPosition().x - getWidth()/2, body.getPosition().y - getHeight()/2 - 8/Mario.PPM);
        }
        else {
            setPosition(body.getPosition().x - getWidth() / 2, body.getPosition().y - getHeight()/2 - 2/Mario.PPM);
        }
        setRegion(getFrames(deltaTime));
        if(timeToDefineBigMario){
            defineBigMario();
        }
        if(timeToRedefineMario){
            redefineMario();
        }
    }

    public void redefineMario(){
        Vector2 position = body.getPosition();
        world.destroyBody(body);

        BodyDef bodyDef = new BodyDef();
        bodyDef.position.set(position);
        bodyDef.type = BodyDef.BodyType.DynamicBody;
        body = world.createBody(bodyDef);
        FixtureDef fixtureDef = new FixtureDef();

        fixtureDef.filter.categoryBits = Mario.MARIO_BIT;
        fixtureDef.filter.maskBits = Mario.GROUND_BIT | Mario.BRICK_BIT | Mario.COIN_BIT | Mario.ENEMY_BIT | Mario.OBJECT_BIT | Mario.ENEMY_HEAD_BIT | Mario.ITEM_BIT;

        EdgeShape feet = new EdgeShape();
        feet.set(new Vector2(-2 / Mario.PPM, -8 / Mario.PPM), new Vector2(2 / Mario.PPM, -8 / Mario.PPM));
        fixtureDef.shape = feet;
        fixtureDef.isSensor = false;
        body.createFixture(fixtureDef).setUserData(this);

        CircleShape shape = new CircleShape();
        shape.setRadius(6/Mario.PPM);
        fixtureDef.shape = shape;
        body.createFixture(fixtureDef).setUserData(this);

        EdgeShape head = new EdgeShape();
        head.set(new Vector2(-2/Mario.PPM, 6/Mario.PPM), new Vector2(2/Mario.PPM, 6/Mario.PPM));
        fixtureDef.filter.categoryBits = Mario.MARIO_HEAD_BIT;
        fixtureDef.shape = head;
        fixtureDef.isSensor = true;
        body.createFixture(fixtureDef).setUserData(this);

        timeToRedefineMario = false;
    }

    private void defineBigMario() {
        Vector2 currentPosition = body.getPosition();
        world.destroyBody(body);

        BodyDef bodyDef = new BodyDef();
        bodyDef.position.set(currentPosition.add(0, 10/Mario.PPM));
        bodyDef.type = BodyDef.BodyType.DynamicBody;
        body = world.createBody(bodyDef);
        FixtureDef fixtureDef = new FixtureDef();

        fixtureDef.filter.categoryBits = Mario.MARIO_BIT;
        fixtureDef.filter.maskBits = Mario.GROUND_BIT | Mario.BRICK_BIT | Mario.COIN_BIT | Mario.ENEMY_BIT | Mario.OBJECT_BIT | Mario.ENEMY_HEAD_BIT | Mario.ITEM_BIT;

        EdgeShape feet = new EdgeShape();
        feet.set(new Vector2(-2 / Mario.PPM, -22 / Mario.PPM), new Vector2(2 / Mario.PPM, -22 / Mario.PPM));
        fixtureDef.shape = feet;
        fixtureDef.isSensor = false;
        body.createFixture(fixtureDef).setUserData(this);

        CircleShape shape = new CircleShape();
        shape.setRadius(6/Mario.PPM);
        fixtureDef.shape = shape;
        body.createFixture(fixtureDef).setUserData(this);
        shape.setPosition(new Vector2(0, -13/Mario.PPM));
        body.createFixture(fixtureDef).setUserData(this);

        EdgeShape head = new EdgeShape();
        head.set(new Vector2(-2/Mario.PPM, 6/Mario.PPM), new Vector2(2/Mario.PPM, 6/Mario.PPM));
        fixtureDef.filter.categoryBits = Mario.MARIO_HEAD_BIT;
        fixtureDef.shape = head;
        fixtureDef.isSensor = true;
        body.createFixture(fixtureDef).setUserData(this);

        timeToDefineBigMario = false;
    }

    public TextureRegion getFrames(float deltaTime){
        currentState = getState();
        TextureRegion region;
        switch (currentState){
            case DEAD:
                region = marioDead;
                break;

            case GROWING:
                region = growMario.getKeyFrame(stateTimer);
                Gdx.app.log("growing", String.valueOf(stateTimer));
                if(growMario.isAnimationFinished(stateTimer)){
                    Gdx.app.log("growing not !!!!", String.valueOf(stateTimer));
                    runGrowAnimation = false;
                }
                break;

            case JUMPING:
                region = marioIsBig ? bigMarioJump : marioJump;
                break;

            case RUNNING:
                region = marioIsBig ? bigMarioRun.getKeyFrame(stateTimer, true) : marioRun.getKeyFrame(stateTimer, true);
                break;

            case FALLING:
            case STANDING:
            default:
                if(runGrowAnimation){
                    region = growMario.getKeyFrame(stateTimer);
                    break;
                }
                region = marioIsBig ? bigMarioStand : marioStand;
                break;
        }
        if((body.getLinearVelocity().x < 0 || !runningRight) && !region.isFlipX()){
            region.flip(true, false);
            runningRight = false;
        } else if((body.getLinearVelocity().x >0 || runningRight) && region.isFlipX()){
            region.flip(true, false);
            runningRight = true;
        }
        if(currentState == previousState){
            stateTimer += deltaTime;
        }
        else {
            stateTimer = 0;
            previousState = currentState;
        }
        return region;
    }

    public State getState(){
        if(marioIsDead){
            return State.DEAD;
        }else if(runGrowAnimation){
            return State.GROWING;
        }else if(body.getLinearVelocity().y > 0 || (body.getLinearVelocity().y < 0 && previousState == State.JUMPING)){
            return State.JUMPING;
        }else if(body.getLinearVelocity().y < 0){
            return State.FALLING;
        }else if(body.getLinearVelocity().x != 0){
            return State.RUNNING;
        }else
            return State.STANDING;
    }

    public void grow(){
        if(!marioIsBig){
            runGrowAnimation = true;
            marioIsBig = true;
            timeToDefineBigMario = true;
            setBounds(getX(), getY(), getWidth(), getHeight() * 2);
            Mario.assetManager.get("music/sounds/powerup.wav", Sound.class).play();
        }
    }

    public boolean isDead(){
        return marioIsDead;
    }

    public float getStateTimer() {
        return stateTimer;
    }

    public void defineMario(){
        BodyDef bodyDef = new BodyDef();
        bodyDef.position.set(32/Mario.PPM, 32/Mario.PPM);
        bodyDef.type = BodyDef.BodyType.DynamicBody;
        body = world.createBody(bodyDef);
        FixtureDef fixtureDef = new FixtureDef();

        fixtureDef.filter.categoryBits = Mario.MARIO_BIT;
        fixtureDef.filter.maskBits = Mario.GROUND_BIT | Mario.BRICK_BIT | Mario.COIN_BIT | Mario.ENEMY_BIT | Mario.OBJECT_BIT | Mario.ENEMY_HEAD_BIT | Mario.ITEM_BIT;

        EdgeShape feet = new EdgeShape();
        feet.set(new Vector2(-2 / Mario.PPM, -8 / Mario.PPM), new Vector2(2 / Mario.PPM, -8 / Mario.PPM));
        fixtureDef.shape = feet;
        fixtureDef.isSensor = false;
        body.createFixture(fixtureDef).setUserData(this);

        CircleShape shape = new CircleShape();
        shape.setRadius(6/Mario.PPM);
        fixtureDef.shape = shape;
        body.createFixture(fixtureDef).setUserData(this);

        EdgeShape head = new EdgeShape();
        head.set(new Vector2(-2/Mario.PPM, 6/Mario.PPM), new Vector2(2/Mario.PPM, 6/Mario.PPM));
        fixtureDef.filter.categoryBits = Mario.MARIO_HEAD_BIT;
        fixtureDef.shape = head;
        fixtureDef.isSensor = true;
        body.createFixture(fixtureDef).setUserData(this);

    }
}
