package com.sparrow.mario.sprites.enemies;

import com.badlogic.gdx.graphics.g2d.Animation;
import com.badlogic.gdx.graphics.g2d.Batch;
import com.badlogic.gdx.graphics.g2d.TextureRegion;
import com.badlogic.gdx.math.Vector2;
import com.badlogic.gdx.physics.box2d.BodyDef;
import com.badlogic.gdx.physics.box2d.CircleShape;
import com.badlogic.gdx.physics.box2d.Filter;
import com.badlogic.gdx.physics.box2d.Fixture;
import com.badlogic.gdx.physics.box2d.FixtureDef;
import com.badlogic.gdx.physics.box2d.PolygonShape;
import com.badlogic.gdx.utils.Array;
import com.sparrow.mario.Mario;
import com.sparrow.mario.screens.PlayScreen;
import com.sparrow.mario.sprites.MarioSprite;

/**
 * Created by sparrow on 21.09.16.
 */

public class Turtle extends Enemy {
    public enum State{WALKING, STANDING_SHELL, MOVING_SHELL, DEAD}
    public State currentState, previousState;
    private float stateTimer;
    private Animation walkAnimation;
    private Array<TextureRegion> frames;
    private TextureRegion shell;
    public static final int KICK_LEFT_SPEED = -2;
    public static final int KICK_RIGHT_SPEED = 2;
    private float deadRotationDegrees;

    public Turtle(PlayScreen playScreen, float x, float y) {
        super(playScreen, x, y);
        frames = new Array<TextureRegion>();
        frames.add(new TextureRegion(playScreen.getTextureAtlas().findRegion("turtle"), 0, 0, 16, 24));
        frames.add(new TextureRegion(playScreen.getTextureAtlas().findRegion("turtle"), 16, 0, 16, 24));
        shell = new TextureRegion(playScreen.getTextureAtlas().findRegion("turtle"), 64, 0, 16, 24);
        walkAnimation = new Animation(0.2f, frames);
        currentState = previousState = State.WALKING;
        setBounds(getX(), getY(), 16/Mario.PPM, 24/Mario.PPM);
    }

    @Override
    protected void defineEnemy() {
        BodyDef bodyDef = new BodyDef();
        bodyDef.position.set(getX(), getY());
        bodyDef.type = BodyDef.BodyType.DynamicBody;
        body = world.createBody(bodyDef);

        FixtureDef fixtureDef = new FixtureDef();
        CircleShape circleShape = new CircleShape();
        circleShape.setRadius(6/Mario.PPM);
        fixtureDef.filter.categoryBits = Mario.ENEMY_BIT;
        fixtureDef.filter.maskBits = Mario.GROUND_BIT |
                Mario.COIN_BIT |
                Mario.BRICK_BIT |
                Mario.ENEMY_BIT |
                Mario.OBJECT_BIT |
                Mario.MARIO_BIT;

        fixtureDef.shape = circleShape;
        body.createFixture(fixtureDef).setUserData(this);

        PolygonShape head = new PolygonShape();
        Vector2[] vertice = new Vector2[4];
        vertice[0] = new Vector2(-5, 8).scl(1/Mario.PPM);
        vertice[1] = new Vector2(5, 8).scl(1/Mario.PPM);
        vertice[2] = new Vector2(-3, 3).scl(1/Mario.PPM);
        vertice[3] = new Vector2(3, 3).scl(1/Mario.PPM);
        head.set(vertice);
        fixtureDef.shape = head;
        fixtureDef.restitution = 1.5f;
        fixtureDef.filter.categoryBits = Mario.ENEMY_HEAD_BIT;
        body.createFixture(fixtureDef).setUserData(this);
    }

    @Override
    public void hitOnHead(MarioSprite marioSprite) {
        if(currentState != State.STANDING_SHELL){
            currentState = State.STANDING_SHELL;
            velocity.x = 0;
        } else {
            kick(marioSprite.getX() <= this.getX() ? KICK_RIGHT_SPEED : KICK_LEFT_SPEED);
        }
    }

    public void kick(int speed){
        velocity.x = speed;
        currentState = State.MOVING_SHELL;
    }

    public State getCurrentState(){
        return currentState;
    }

    @Override
    public void update(float deltaTime) {
        setRegion(getFrame(deltaTime));
        if(currentState == State.STANDING_SHELL && stateTimer > 5){
            currentState = State.WALKING;
            velocity.x = 1;
        }
        setPosition(body.getPosition().x - getWidth()/2, body.getPosition().y - 8/Mario.PPM);

        if(currentState == State.DEAD){
            deadRotationDegrees += 3;
            rotate(deadRotationDegrees);
            if(stateTimer > 5 && !destroyed){
                world.destroyBody(body);
                destroyed = true;
            }
        }
        else {
            body.setLinearVelocity(velocity);
        }
    }

    @Override
    public void onEnemyHit(Enemy enemy) {
        if(enemy instanceof Turtle){
            if(((Turtle)enemy).currentState == State.MOVING_SHELL && currentState != State.MOVING_SHELL){
                killed();
            }
            else if(currentState == State.MOVING_SHELL && ((Turtle)enemy).currentState == State.WALKING){
                return;
            }
            else {
                reverseVelocity(true, false);
            }
        }
        else if(currentState == State.WALKING){
            reverseVelocity(true, false);
        }
    }

    private TextureRegion getFrame(float deltaTime) {
        TextureRegion textureRegion;
        switch (currentState){
            case MOVING_SHELL:
            case STANDING_SHELL:
                textureRegion = shell;
                break;

            case WALKING:
            default:
                textureRegion = walkAnimation.getKeyFrame(stateTimer, true);
                break;
        }

        if(velocity.x > 0 && textureRegion.isFlipX() == false){
            textureRegion.flip(true, false);
        }
        if(velocity.x < 0 && textureRegion.isFlipX() == true){
            textureRegion.flip(true, false);
        }
        if(currentState == previousState){
            stateTimer += deltaTime;
        }
        else {
            stateTimer = 0;
            previousState = currentState;
        }
        return textureRegion;
    }

    public void killed(){
        currentState = State.DEAD;
        Filter filter = new Filter();
        filter.maskBits = Mario.NOTHING_BIT;

        for(Fixture fixture : body.getFixtureList()){
            fixture.setFilterData(filter);
        }
        body.applyLinearImpulse(new Vector2(0, 5f), body.getWorldCenter(), true);
    }

    public void draw(Batch batch){
        if(!destroyed){
            super.draw(batch);
        }
    }
}
