package com.sparrow.mario.sprites.enemies;

import com.badlogic.gdx.graphics.g2d.Sprite;
import com.badlogic.gdx.math.Vector2;
import com.badlogic.gdx.physics.box2d.Body;
import com.badlogic.gdx.physics.box2d.World;
import com.sparrow.mario.screens.PlayScreen;
import com.sparrow.mario.sprites.MarioSprite;

/**
 * Created by sparrow on 12.09.16.
 */
public abstract class Enemy extends Sprite {
    protected World world;
    protected PlayScreen playScreen;
    public Body body;
    protected boolean setToDestroy, destroyed;
    public Vector2 velocity;

    public Enemy(PlayScreen playScreen, float x, float y){
        setPosition(x, y);
        this.world = playScreen.getWorld();
        this.playScreen = playScreen;
        defineEnemy();
        velocity = new Vector2(-1, -2);
        body.setActive(false);
    }

    protected abstract void defineEnemy();
    public abstract void hitOnHead(MarioSprite marioSprite);
    public abstract void update(float deltaTime);
    public abstract void onEnemyHit(Enemy enemy);

    public void reverseVelocity(boolean x, boolean y){
        if(x){
            velocity.x = -velocity.x;
        }
        if(y){
            velocity.y = -velocity.y;
        }
    }
}
