package com.sparrow.mario.sprites.enemies;

import com.badlogic.gdx.audio.Sound;
import com.badlogic.gdx.graphics.g2d.Animation;
import com.badlogic.gdx.graphics.g2d.Batch;
import com.badlogic.gdx.graphics.g2d.TextureRegion;
import com.badlogic.gdx.math.Vector2;
import com.badlogic.gdx.physics.box2d.BodyDef;
import com.badlogic.gdx.physics.box2d.CircleShape;
import com.badlogic.gdx.physics.box2d.FixtureDef;
import com.badlogic.gdx.physics.box2d.PolygonShape;
import com.badlogic.gdx.utils.Array;
import com.sparrow.mario.Mario;
import com.sparrow.mario.screens.PlayScreen;
import com.sparrow.mario.sprites.MarioSprite;

/**
 * Created by sparrow on 12.09.16.
 */
public class Goomba extends Enemy {
    private float stateTime;
    private Animation walkAnimation;
    private Array<TextureRegion> frames;

    public Goomba(PlayScreen playScreen, float x, float y) {
        super(playScreen, x, y);
        frames = new Array<TextureRegion>();
        for(int i=0; i<2; ++i){
            frames.add(new TextureRegion(playScreen.getTextureAtlas().findRegion("goomba"), i*16, 0, 16, 16));
        }
        walkAnimation = new Animation(0.4f, frames);
        stateTime = 0;
        setBounds(getX(), getY(), 16/Mario.PPM, 16/Mario.PPM);
    }

    public void update(float deltaTime){
        stateTime += deltaTime;
        if(setToDestroy && !destroyed){
            world.destroyBody(body);
            destroyed = true;
            setRegion(new TextureRegion(playScreen.getTextureAtlas().findRegion("goomba"), 32, 0, 16, 16));
            stateTime=0;
        }else if(!destroyed) {

            body.setLinearVelocity(velocity);
            setPosition(body.getPosition().x - getWidth() / 2, body.getPosition().y - getHeight() / 2);
            setRegion(walkAnimation.getKeyFrame(stateTime, true));
            }
        }

    @Override
    public void onEnemyHit(Enemy enemy) {
        if(enemy instanceof Turtle && ((Turtle)enemy).currentState == Turtle.State.MOVING_SHELL){
            setToDestroy = true;
        }
        else {
            reverseVelocity(true, false);
        }
    }

    @Override
    protected void defineEnemy() {
        BodyDef bodyDef = new BodyDef();
        bodyDef.position.set(getX(), getY());
        bodyDef.type = BodyDef.BodyType.DynamicBody;
        body = world.createBody(bodyDef);

        FixtureDef fixtureDef = new FixtureDef();
        CircleShape circleShape = new CircleShape();
        circleShape.setRadius(6/Mario.PPM);
        fixtureDef.filter.categoryBits = Mario.ENEMY_BIT;
        fixtureDef.filter.maskBits = Mario.GROUND_BIT |
                Mario.COIN_BIT |
                Mario.BRICK_BIT |
                Mario.ENEMY_BIT |
                Mario.OBJECT_BIT |
                Mario.MARIO_BIT;

        fixtureDef.shape = circleShape;
        body.createFixture(fixtureDef).setUserData(this);

        PolygonShape head = new PolygonShape();
        Vector2[] vertice = new Vector2[4];
        vertice[0] = new Vector2(-5, 8).scl(1/Mario.PPM);
        vertice[1] = new Vector2(5, 8).scl(1/Mario.PPM);
        vertice[2] = new Vector2(-3, 3).scl(1/Mario.PPM);
        vertice[3] = new Vector2(3, 3).scl(1/Mario.PPM);
        head.set(vertice);
        fixtureDef.shape = head;
        fixtureDef.restitution = 0.5f;
        fixtureDef.filter.categoryBits = Mario.ENEMY_HEAD_BIT;
        body.createFixture(fixtureDef).setUserData(this);

    }

    @Override
    public void draw(Batch batch) {
        if(!destroyed || stateTime<0.5){
            super.draw(batch);
        }
    }

    @Override
    public void hitOnHead(MarioSprite marioSprite) {
        setToDestroy = true;
        Mario.assetManager.get("music/sounds/stomp.wav", Sound.class).play();
    }
}
