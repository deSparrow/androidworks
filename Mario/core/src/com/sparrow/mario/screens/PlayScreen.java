package com.sparrow.mario.screens;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.Input;
import com.badlogic.gdx.Screen;
import com.badlogic.gdx.audio.Music;
import com.badlogic.gdx.graphics.GL20;
import com.badlogic.gdx.graphics.OrthographicCamera;
import com.badlogic.gdx.graphics.g2d.TextureAtlas;
import com.badlogic.gdx.maps.tiled.TiledMap;
import com.badlogic.gdx.maps.tiled.TmxMapLoader;
import com.badlogic.gdx.maps.tiled.renderers.OrthogonalTiledMapRenderer;
import com.badlogic.gdx.math.Vector2;
import com.badlogic.gdx.physics.box2d.Box2DDebugRenderer;
import com.badlogic.gdx.physics.box2d.World;
import com.badlogic.gdx.utils.Array;
import com.badlogic.gdx.utils.viewport.FitViewport;
import com.badlogic.gdx.utils.viewport.Viewport;
import com.sparrow.mario.Mario;
import com.sparrow.mario.scenes.Hud;
import com.sparrow.mario.sprites.enemies.Enemy;
import com.sparrow.mario.sprites.MarioSprite;
import com.sparrow.mario.sprites.items.Item;
import com.sparrow.mario.sprites.items.ItemDefinition;
import com.sparrow.mario.sprites.items.Mushroom;
import com.sparrow.mario.tools.WorldContactListener;
import com.sparrow.mario.tools.WorldCreator;

import java.util.PriorityQueue;
import java.util.concurrent.LinkedBlockingQueue;

/**
 * Created by sparrow on 06.09.16.
 */
public class PlayScreen implements Screen {
    private Mario game;
    private OrthographicCamera gameCamera;
    private Viewport gamePort;
    private Hud hud;
    private TmxMapLoader mapLoader;
    private TiledMap tiledMap;
    private OrthogonalTiledMapRenderer mapRenderer;

    //box2d
    private World world;
    private Box2DDebugRenderer debugRenderer;

    private MarioSprite player;

    private TextureAtlas textureAtlas;

    private Music music;

    private WorldCreator worldCreator;

    private Array<Item> items;
    private LinkedBlockingQueue<ItemDefinition> itemsToSpawn;

    public PlayScreen(Mario game){
        textureAtlas = new TextureAtlas("Mario_and_Enemies.pack");
        this.game = game;
        gameCamera = new OrthographicCamera();
        gamePort = new FitViewport(Mario.V_WIDTH/ Mario.PPM, Mario.V_HEIGHT/Mario.PPM, gameCamera);
        hud = new Hud(game.batch);

        mapLoader = new TmxMapLoader();
        tiledMap = mapLoader.load("level1.tmx");
        mapRenderer = new OrthogonalTiledMapRenderer(tiledMap, 1/Mario.PPM);
        gameCamera.position.set(gamePort.getWorldWidth()/2, gamePort.getWorldHeight()/2, 0);

        world = new World(new Vector2(0, -10), true);
        debugRenderer = new Box2DDebugRenderer();

        worldCreator = new WorldCreator(this);

        player = new MarioSprite(this);

        world.setContactListener(new WorldContactListener());

        music = Mario.assetManager.get("music/theme/theme_music.ogg", Music.class);
        music.setLooping(true);
        //music.play();

        items = new Array<Item>();
        itemsToSpawn = new LinkedBlockingQueue<ItemDefinition>();


    }

    public void spawnItem(ItemDefinition itemDefinition){
        itemsToSpawn.add(itemDefinition);
    }

    public void handleSpawningItems(){
        if(!itemsToSpawn.isEmpty()){
            ItemDefinition itemDefinition = itemsToSpawn.poll();
            if(itemDefinition.type == Mushroom.class){
                items.add(new Mushroom(this, itemDefinition.position.x, itemDefinition.position.y));
            }
        }
    }

    public void update(float deltaTime){
        handleInput(deltaTime);
        handleSpawningItems();

        world.step(1/60f, 6, 2);
        player.update(deltaTime);

        for(Enemy enemy : worldCreator.getEnemies()){
            enemy.update(deltaTime);
            if(enemy.getX() < player.getX()+256/Mario.PPM){
                enemy.body.setActive(true);
            }
        }

        for(Item item : items){
            item.update(deltaTime);
        }

        hud.update(deltaTime);
        if(player.currentState != MarioSprite.State.DEAD) {
            gameCamera.position.x = player.body.getPosition().x;
        }
        gameCamera.update();
        mapRenderer.setView(gameCamera);
    }

    public TextureAtlas getTextureAtlas(){
        return textureAtlas;
    }

    public void handleInput(float deltaTime){
        if(player.currentState != MarioSprite.State.DEAD) {
            if (Gdx.input.isKeyJustPressed(Input.Keys.UP)) {
                player.body.applyLinearImpulse(new Vector2(0, 4f), player.body.getWorldCenter(), true);
            }
            if (Gdx.input.isKeyPressed(Input.Keys.RIGHT) && player.body.getLinearVelocity().x <= 2) {
                player.body.applyLinearImpulse(new Vector2(0.1f, 0), player.body.getWorldCenter(), true);
            }
            if (Gdx.input.isKeyPressed(Input.Keys.LEFT) && player.body.getLinearVelocity().x >= -2) {
                player.body.applyLinearImpulse(new Vector2(-0.1f, 0), player.body.getWorldCenter(), true);
            }
        }
    }

    public boolean gameOver(){
        if(player.currentState == MarioSprite.State.DEAD && player.getStateTimer() > 3){
            return true;
        }
        return false;
    }

    @Override
    public void show() {

    }

    @Override
    public void render(float delta) {
        update(delta);

        Gdx.gl.glClearColor(0,0,0,0);
        Gdx.gl.glClear(GL20.GL_COLOR_BUFFER_BIT);

        mapRenderer.render();
        debugRenderer.render(world, gameCamera.combined);

        game.batch.setProjectionMatrix(gameCamera.combined);
        game.batch.begin();
        player.draw(game.batch);
        for(Enemy enemy : worldCreator.getEnemies()){
            enemy.draw(game.batch);
        }
        for(Item item : items){
            item.draw(game.batch);
        }

        game.batch.end();

        game.batch.setProjectionMatrix(hud.stage.getCamera().combined);
        hud.stage.draw();

        if(gameOver()){
            game.setScreen(new GameOverScreen(game));
            dispose();
        }

    }

    @Override
    public void resize(int width, int height) {
        gamePort.update(width, height);
    }

    public TiledMap getTiledMap(){
        return tiledMap;
    }

    public World getWorld(){
        return world;
    }

    @Override
    public void pause() {

    }

    @Override
    public void resume() {

    }

    @Override
    public void hide() {

    }

    @Override
    public void dispose() {
        tiledMap.dispose();
        mapRenderer.dispose();
        world.dispose();
        debugRenderer.dispose();
        hud.dispose();
    }
}
