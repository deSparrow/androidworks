package com.sparrow.mario.tools;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.physics.box2d.Contact;
import com.badlogic.gdx.physics.box2d.ContactImpulse;
import com.badlogic.gdx.physics.box2d.ContactListener;
import com.badlogic.gdx.physics.box2d.Fixture;
import com.badlogic.gdx.physics.box2d.Manifold;
import com.sparrow.mario.Mario;
import com.sparrow.mario.sprites.MarioSprite;
import com.sparrow.mario.sprites.enemies.Enemy;
import com.sparrow.mario.sprites.items.Item;
import com.sparrow.mario.sprites.tileObjects.InteractiveTileObject;

/**
 * Created by sparrow on 10.09.16.
 */
public class WorldContactListener implements ContactListener {

    public WorldContactListener() {
    }

    @Override
    public void beginContact(Contact contact) {
        Fixture fixtureA = contact.getFixtureA();
        Fixture fixtureB = contact.getFixtureB();

        int colisionDef = fixtureA.getFilterData().categoryBits | fixtureB.getFilterData().categoryBits;

        switch (colisionDef){
            case Mario.MARIO_HEAD_BIT | Mario.BRICK_BIT:
            case Mario.MARIO_HEAD_BIT | Mario.COIN_BIT:
                if(fixtureA.getFilterData().categoryBits == Mario.MARIO_HEAD_BIT){
                    ((InteractiveTileObject)fixtureB.getUserData()).onHeadHit((MarioSprite)fixtureA.getUserData());
                }
                else {
                    ((InteractiveTileObject)fixtureA.getUserData()).onHeadHit((MarioSprite)fixtureB.getUserData());
                }
                break;

            case Mario.ENEMY_HEAD_BIT | Mario.MARIO_BIT:
                if(fixtureA.getFilterData().categoryBits == Mario.ENEMY_HEAD_BIT){
                    ((Enemy)fixtureA.getUserData()).hitOnHead((MarioSprite)fixtureB.getUserData());
                }else{
                    ((Enemy)fixtureB.getUserData()).hitOnHead((MarioSprite)fixtureA.getUserData());
                }
                break;

            case Mario.ENEMY_BIT | Mario.OBJECT_BIT:
                if(fixtureA.getFilterData().categoryBits == Mario.ENEMY_BIT){
                    ((Enemy)fixtureA.getUserData()).reverseVelocity(true, false);
                }else{
                    ((Enemy)fixtureB.getUserData()).reverseVelocity(true, false);
                }
                break;

            case Mario.MARIO_BIT | Mario.ENEMY_BIT:
                if(fixtureA.getFilterData().categoryBits == Mario.MARIO_BIT){
                    ((MarioSprite)fixtureA.getUserData()).hit((Enemy)fixtureB.getUserData());
                }
                else {
                    ((MarioSprite)fixtureB.getUserData()).hit((Enemy)fixtureA.getUserData());
                }
                break;

            case Mario.ENEMY_BIT | Mario.ENEMY_BIT:
                ((Enemy)fixtureA.getUserData()).onEnemyHit((Enemy)fixtureB.getUserData());
                ((Enemy)fixtureB.getUserData()).onEnemyHit((Enemy)fixtureA.getUserData());
                break;

            case Mario.ITEM_BIT | Mario.OBJECT_BIT:
                if(fixtureA.getFilterData().categoryBits == Mario.ITEM_BIT){
                    ((Item)fixtureA.getUserData()).reverseVelocity(true, false);
                }else{
                    ((Item)fixtureB.getUserData()).reverseVelocity(true, false);
                }
                break;

            case Mario.ITEM_BIT | Mario.MARIO_BIT:
                    if(fixtureA.getFilterData().categoryBits == Mario.ITEM_BIT)
                    {
                        ((Item)fixtureA.getUserData()).use((MarioSprite)fixtureB.getUserData());
                        fixtureA.getBody().setLinearVelocity(0, 0);
                    }
                    else
                    {
                        ((Item) fixtureB.getUserData()).use((MarioSprite) fixtureA.getUserData());
                        fixtureB.getBody().setLinearVelocity(0, 0);
                    }
                break;

        }
    }

    @Override
    public void endContact(Contact contact) {

    }

    @Override
    public void preSolve(Contact contact, Manifold oldManifold) {

    }

    @Override
    public void postSolve(Contact contact, ContactImpulse impulse) {

    }
}
