package com.sparrow.mario.tools;

import com.badlogic.gdx.maps.MapObject;
import com.badlogic.gdx.maps.objects.RectangleMapObject;
import com.badlogic.gdx.maps.tiled.TiledMap;
import com.badlogic.gdx.math.Rectangle;
import com.badlogic.gdx.physics.box2d.Body;
import com.badlogic.gdx.physics.box2d.BodyDef;
import com.badlogic.gdx.physics.box2d.FixtureDef;
import com.badlogic.gdx.physics.box2d.PolygonShape;
import com.badlogic.gdx.physics.box2d.World;
import com.badlogic.gdx.utils.Array;
import com.sparrow.mario.Mario;
import com.sparrow.mario.screens.PlayScreen;
import com.sparrow.mario.sprites.enemies.Enemy;
import com.sparrow.mario.sprites.enemies.Turtle;
import com.sparrow.mario.sprites.tileObjects.BrickSprite;
import com.sparrow.mario.sprites.tileObjects.CoinSprite;
import com.sparrow.mario.sprites.enemies.Goomba;

/**
 * Created by sparrow on 09.09.16.
 */
public class WorldCreator {
    private Array<Goomba> goombas;
    private Array<Turtle> turtles;

    public WorldCreator(PlayScreen playScreen){
        World world = playScreen.getWorld();
        TiledMap tiledMap = playScreen.getTiledMap();
        BodyDef bodyDef = new BodyDef();
        PolygonShape shape = new PolygonShape();
        FixtureDef fixtureDef = new FixtureDef();
        Body body;

        //ground
        for(MapObject object : tiledMap.getLayers().get(2).getObjects().getByType(RectangleMapObject.class)){
            Rectangle rectangle = ((RectangleMapObject)object).getRectangle();
            bodyDef.type = BodyDef.BodyType.StaticBody;
            bodyDef.position.set((rectangle.getX() + rectangle.getWidth()/2)/ Mario.PPM, (rectangle.getY() + rectangle.getHeight()/2)/Mario.PPM);
            body = world.createBody(bodyDef);
            shape.setAsBox(rectangle.getWidth()/2/Mario.PPM, rectangle.getHeight()/2/Mario.PPM);
            fixtureDef.shape = shape;
            body.createFixture(fixtureDef);
        }

        //pipes
        for(MapObject object : tiledMap.getLayers().get(3).getObjects().getByType(RectangleMapObject.class)){
            Rectangle rectangle = ((RectangleMapObject)object).getRectangle();
            bodyDef.type = BodyDef.BodyType.StaticBody;
            bodyDef.position.set((rectangle.getX() + rectangle.getWidth()/2)/Mario.PPM, (rectangle.getY() + rectangle.getHeight()/2)/Mario.PPM);
            body = world.createBody(bodyDef);
            shape.setAsBox(rectangle.getWidth()/2/Mario.PPM, rectangle.getHeight()/2/Mario.PPM);
            fixtureDef.shape = shape;
            fixtureDef.filter.categoryBits = Mario.OBJECT_BIT;
            body.createFixture(fixtureDef);
        }

        //bricks
        for(MapObject object : tiledMap.getLayers().get(5).getObjects().getByType(RectangleMapObject.class)){
            new BrickSprite(playScreen, object);
        }

        //coins
        for(MapObject object : tiledMap.getLayers().get(4).getObjects().getByType(RectangleMapObject.class)){
            new CoinSprite(playScreen, object);
        }

        //goombas
        goombas = new Array<Goomba>();
        for(RectangleMapObject object : tiledMap.getLayers().get(6).getObjects().getByType(RectangleMapObject.class)){
            Rectangle rectangle = object.getRectangle();
            goombas.add(new Goomba(playScreen, rectangle.getX()/Mario.PPM, rectangle.getY()/Mario.PPM));
        }

        turtles = new Array<Turtle>();
        for(RectangleMapObject object : tiledMap.getLayers().get(7).getObjects().getByType(RectangleMapObject.class)){
            Rectangle rectangle = object.getRectangle();
            turtles.add(new Turtle(playScreen, rectangle.getX()/Mario.PPM, rectangle.getY()/Mario.PPM));
        }
    }

    public Array<Goomba> getGoombas() {
        return goombas;
    }

    public Array<Enemy> getEnemies(){
        Array<Enemy> enemies = new Array<Enemy>();
        enemies.addAll(goombas);
        enemies.addAll(turtles);
        return enemies;
    }
}
