package com.sparrow.vballs.desktop;

import com.badlogic.gdx.backends.lwjgl.LwjglApplication;
import com.badlogic.gdx.backends.lwjgl.LwjglApplicationConfiguration;
import com.sparrow.vballs.ViciousBallsGame;
import com.sparrow.vballs.config.GlobalConst;

public class DesktopLauncher {
	public static void main (String[] arg) {
		LwjglApplicationConfiguration config = new LwjglApplicationConfiguration();
		config.height = GlobalConst.SCREEN_HEIGHT;
		config.width = GlobalConst.SCREEN_WIDTH;
		new LwjglApplication(new ViciousBallsGame(), config);
	}
}
