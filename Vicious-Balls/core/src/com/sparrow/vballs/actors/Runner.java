package com.sparrow.vballs.actors;

import com.badlogic.gdx.physics.box2d.Body;
import com.badlogic.gdx.physics.box2d.PolygonShape;
import com.sparrow.vballs.config.GlobalConst;
import com.sparrow.vballs.enums.RunnerState;
import com.sparrow.vballs.physics.RunnerUserData;

/**
 * Created by sparrow on 17.04.17.
 */

public class Runner extends GameActor {
    private RunnerState runnerState;

    public Runner(Body body) {
        super(body);
        runnerState = RunnerState.RUNNING;
    }

    @Override
    public RunnerUserData getUserData() {
        return (RunnerUserData) userData;
    }

    public void jump(){
        if(runnerState == RunnerState.RUNNING){
            body.applyLinearImpulse(getUserData().getJumpingLinearImpulse(), body.getWorldCenter(), true);
            runnerState = RunnerState.JUMPING;
        }
    }

    public void dodge(){
        if(runnerState == RunnerState.RUNNING){
            ((PolygonShape)body.getFixtureList().get(0).getShape()).setAsBox(GlobalConst.RUNNER_WIDTH/2, GlobalConst.RUNNER_HEIGHT/4);
            body.setTransform(getUserData().getDodgingPosition(), 0);
            runnerState = RunnerState.DODGING;
        }
    }

    public void stopDodge(){
        ((PolygonShape)body.getFixtureList().get(0).getShape()).setAsBox(GlobalConst.RUNNER_WIDTH/2, GlobalConst.RUNNER_HEIGHT/2);
        body.setTransform(getUserData().getRunningPosition(), 0);
        runnerState = RunnerState.RUNNING;
    }

    public void landed(){
        if(runnerState == RunnerState.JUMPING){
            runnerState = RunnerState.RUNNING;
        }
    }

    public boolean isDoging(){
        return runnerState == RunnerState.DODGING;
    }

    public void hit() {
        body.applyAngularImpulse(getUserData().getHitAngularImpulse(), true);
        runnerState = RunnerState.HIT;
    }

    public boolean isHit() {
        return runnerState == RunnerState.HIT;
    }

}
