package com.sparrow.vballs.actors;

import com.badlogic.gdx.physics.box2d.Body;
import com.sparrow.vballs.physics.EnemyUserData;

/**
 * Created by sparrow on 01.05.17.
 */

public class Enemy extends GameActor {

    public Enemy(Body body) {
        super(body);
    }

    @Override
    public EnemyUserData getUserData() {
        return (EnemyUserData) userData;
    }

    @Override
    public void act(float deltaTime) {
        super.act(deltaTime);
        body.setLinearVelocity(getUserData().getLinearVelocity());
    }

}