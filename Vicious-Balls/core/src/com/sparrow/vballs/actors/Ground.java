package com.sparrow.vballs.actors;

import com.badlogic.gdx.physics.box2d.Body;
import com.sparrow.vballs.physics.GroundUserData;
import com.sparrow.vballs.physics.UserData;

/**
 * Created by sparrow on 17.04.17.
 */

public class Ground extends GameActor {
    public Ground(Body body) {
        super(body);
    }

    @Override
    public UserData getUserData() {
        return (GroundUserData)userData;
    }
}
