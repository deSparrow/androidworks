package com.sparrow.vballs.physics;

import com.sparrow.vballs.enums.UserDataType;

/**
 * Created by sparrow on 17.04.17.
 */

public class GroundUserData extends UserData {
    public GroundUserData() {
        super();
        userDataType = UserDataType.GROUND;
    }
}
