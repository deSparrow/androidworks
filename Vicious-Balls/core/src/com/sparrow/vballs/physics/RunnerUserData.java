package com.sparrow.vballs.physics;

import com.badlogic.gdx.math.Vector2;
import com.sparrow.vballs.config.GlobalConst;
import com.sparrow.vballs.enums.UserDataType;

/**
 * Created by sparrow on 17.04.17.
 */

public class RunnerUserData extends UserData {
    private Vector2 jumpingLinearImpulse;
    private final Vector2 runningPosition = new Vector2(GlobalConst.RUNNER_X, GlobalConst.RUNNER_Y);
    private final Vector2 dodgingPosition = new Vector2(GlobalConst.RUNNER_X, GlobalConst.RUNNER_Y-0.5f);

    public RunnerUserData(float width, float height) {
        super(width, height);
        jumpingLinearImpulse = GlobalConst.RUNNER_JUMPING_LINEAR_IMPULSE;
        userDataType = UserDataType.RUNNER;
    }

    public Vector2 getJumpingLinearImpulse() {
        return jumpingLinearImpulse;
    }

    public void setJumpingLinearImpulse(Vector2 jumpingLinearImpulse) {
        this.jumpingLinearImpulse = jumpingLinearImpulse;
    }

    public Vector2 getRunningPosition() {
        return runningPosition;
    }

    public Vector2 getDodgingPosition() {
        return dodgingPosition;
    }

    public float getHitAngularImpulse() {
        return GlobalConst.RUNNER_HIT_ANGULAR_IMPULSE;
    }
}
