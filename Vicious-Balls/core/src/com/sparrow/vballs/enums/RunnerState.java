package com.sparrow.vballs.enums;

/**
 * Created by sparrow on 17.04.17.
 */

public enum RunnerState {
    RUNNING,
    JUMPING,
    DODGING,
    HIT
}
