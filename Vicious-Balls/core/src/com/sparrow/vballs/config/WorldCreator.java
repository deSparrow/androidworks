package com.sparrow.vballs.config;

import com.badlogic.gdx.math.Vector2;
import com.badlogic.gdx.physics.box2d.Body;
import com.badlogic.gdx.physics.box2d.BodyDef;
import com.badlogic.gdx.physics.box2d.PolygonShape;
import com.badlogic.gdx.physics.box2d.World;
import com.sparrow.vballs.enums.EnemyType;
import com.sparrow.vballs.physics.EnemyUserData;
import com.sparrow.vballs.physics.GroundUserData;
import com.sparrow.vballs.physics.RunnerUserData;
import com.sparrow.vballs.utils.RandomUtils;

/**
 * Created by sparrow on 14.04.17.
 */

public class WorldCreator {
    public static World createWorld(){
        return new World(GlobalConst.WORLD_GRAVITY, true);
    }

    public static Body createGround(World world){
        BodyDef bodyDef = new BodyDef();
        bodyDef.position.set(new Vector2(GlobalConst.GROUND_X, GlobalConst.GROUND_Y));
        Body body = world.createBody(bodyDef);
        PolygonShape polygonShape = new PolygonShape();
        polygonShape.setAsBox(GlobalConst.GROUND_WIDTH, GlobalConst.GROUND_HEIGHT/2);
        body.createFixture(polygonShape, GlobalConst.GROUND_DENSITY);
        body.setUserData(new GroundUserData());
        polygonShape.dispose();
        return body;
    }

    public static Body createRunner(World world){
        BodyDef bodyDef = new BodyDef();
        bodyDef.type = BodyDef.BodyType.DynamicBody;
        bodyDef.position.set(new Vector2(GlobalConst.RUNNER_X, GlobalConst.RUNNER_Y+0.01f));
        PolygonShape polygonShape = new PolygonShape();
        polygonShape.setAsBox(GlobalConst.RUNNER_WIDTH/2, GlobalConst.RUNNER_HEIGHT/2);
        Body body = world.createBody(bodyDef);
        body.setGravityScale(GlobalConst.RUNNER_GRAVITY_SCALE);
        body.createFixture(polygonShape, GlobalConst.RUNNER_DENSITY);
        body.resetMassData();
        body.setUserData(new RunnerUserData(GlobalConst.RUNNER_WIDTH, GlobalConst.RUNNER_HEIGHT));
        polygonShape.dispose();
        return body;
    }

    public static Body createEnemy(World world) {
        EnemyType enemyType = RandomUtils.getRandomEnemyType();
        BodyDef bodyDef = new BodyDef();
        bodyDef.type = BodyDef.BodyType.KinematicBody;
        bodyDef.position.set(new Vector2(enemyType.getX(), enemyType.getY()));
        PolygonShape shape = new PolygonShape();
        shape.setAsBox(enemyType.getWidth() / 2, enemyType.getHeight() / 2);
        Body body = world.createBody(bodyDef);
        body.createFixture(shape, enemyType.getDensity());
        body.resetMassData();
        EnemyUserData userData = new EnemyUserData(enemyType.getWidth(), enemyType.getHeight());
        body.setUserData(userData);
        shape.dispose();
        return body;
    }
}
