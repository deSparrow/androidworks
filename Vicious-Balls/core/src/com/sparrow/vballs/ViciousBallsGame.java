package com.sparrow.vballs;

import com.badlogic.gdx.Game;
import com.sparrow.vballs.screens.GameScreen;

public class ViciousBallsGame extends Game {

	@Override
	public void create() {
		setScreen(new GameScreen());
	}
}
